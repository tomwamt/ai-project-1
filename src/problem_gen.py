from random import random as randfloat, choice as randchoice

def dist_sq(p1, p2):
	'''Returns the squared distance between 2 points'''
	return (p1[0] - p2[0])**2 + (p1[1] - p2[1])**2

def sub(v, w):
	'''Returns v minus w'''
	return (v[0] - w[0], v[1] - w[1])

def cross(v, w):
	'''returns the two dimensional "cross product" of v and w'''
	return v[0]*w[1] - v[1]*w[0]

# algorithm adapted from StackOVerflow answer:
# http://stackoverflow.com/a/565282
# See that answer for details
def intersect(l1a, l1b, l2a, l2b):
	'''Returns True if the line l1 = (l1a, l1b) intersects line l2 = (l2a, l2b), False otherwise'''
	p = l1a
	r = sub(l1b, l1a)
	q = l2a
	s = sub(l2b, l2a)

	qsp = sub(q, p)
	rcs = cross(r, s)

	if abs(rcs) < 1e-9: return False # Close enough to zero to not matter

	t = cross(qsp, s) / rcs
	u = cross(qsp, r) / rcs

	return t > 0 and t < 1 and u > 0 and u < 1

def generate_graph(n):
	'''Generates a planar graph with n vertices'''

	# Generate n random veritces
	v = sorted([(randfloat(), randfloat()) for i in range(n)], key=lambda e: e[0])
	# Generate an adjacency matrix with no edges
	e = [[False]*n for i in range(n)]

	canConnect = list(v) # list of vertices that might still connect to another
	while len(canConnect) > 0:
		p1 = randchoice(canConnect) # randomly select a vertex
		i = v.index(p1) # Get its index in the master list
		j = None
		minD = 100.0 # arbitrary large number for finding a min
		for k in range(n):
			p2 = v[k]
			d = dist_sq(p1, p2)
			if i != k and d < minD and not e[i][k] and not any((intersect(v[a], v[b], p1, p2) for a in range(n) for b in range(a) if e[a][b])): # Holy one-liner Batman!
				minD = d # new closest vertex found
				j = k

		if j is None: # No vertices were found to connect to, remove from possibilites
			canConnect.remove(p1)
		else: # Otherwise, connect them
			e[i][j] = True
			e[j][i] = True

	return (v, e)

def draw_colored_graph(v, e, colors=None, filename='graph.png'):
	try:
		from PIL import Image, ImageDraw

		drawcolors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255), (0, 255, 255)]
		size = 1024
		img = Image.new('RGB', (size, size), (0, 0, 0))
		draw = ImageDraw.Draw(img)

		n = len(v)

		for i, j in ((i, j) for i in range(n) for j in range(i) if e[i][j]):
			draw.line([v[i][0] * size, v[i][1] * size, v[j][0] * size, v[j][1] * size], fill=(255, 255, 255))

		for i in range(n):
			p = v[i]
			color = None
			if colors is None:
				color = (255, 255, 255)
			else:
				color = drawcolors[colors[i]]
			draw.ellipse([p[0]*size - 5, p[1]*size - 5, p[0]*size + 5, p[1]*size + 5], fill=color)

		img.save(filename)
	except ImportError:
		print('PIL (Python Imaging Library) required to draw graphs')

def load_graph(filename):
	f = open(filename)
	lines = f.readlines()
	f.close()

	n = int(lines.pop(0))
	v = []
	e = []

	for i in range(n):
		vertex = tuple(float(p) for p in lines.pop(0).split(' '))
		v.append(vertex)

	for i in range(n):
		row = [True if p == '1' else False for p in lines.pop(0).split(' ')]
		e.append(row)

	return (v, e)

if __name__ == '__main__':
	import sys
	args = sys.argv

	n = 10
	if len(args) > 1:
		n = int(args[1])

	# generate graph
	v, e = generate_graph(n)

	#export as a text file
	f = open('graph.txt', 'w')
	f.write(str(n) + '\n')
	for point in v:
		f.write(str(point[0]) + ' ' + str(point[1]) + '\n')
	for row in e:
		f.write(' '.join(('1' if x else '0' for x in row)) + '\n')
	f.close()

	try:
		from PIL import Image, ImageDraw
		# Draw the graph
		size = 512
		img = Image.new('RGB', (size, size), (0, 0, 0))
		draw = ImageDraw.Draw(img)

		for i, j in ((i, j) for i in range(n) for j in range(i) if e[i][j]):
			draw.line([v[i][0] * size, v[i][1] * size, v[j][0] * size, v[j][1] * size], fill=(255, 50, 50))

		for p in v:
			draw.ellipse([p[0]*size - 2, p[1]*size - 2, p[0]*size + 2, p[1]*size + 2], fill=(255, 255, 255))

		img.save('graph.png')
	except ImportError:
		print('Install PIL for visualization functionality')