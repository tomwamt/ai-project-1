from multiprocessing import Pool
import problem_gen
from random import randrange, choice as randchoice, random, sample as randsample
from math import log2

OUTPUT = False

def genetic(n, e, k, maxIters, popSize, mutateChance, tournSize, tournChance):
	calls = {
		'conflicts': 0,
		'select': 0,
		'reproduce': 0,
		'mutate': 0,
	}

	def print_calls():
		if OUTPUT:
			print('\nMethod calls:')
			print('\tconflicts:', calls['conflicts'])
			print('\ttournament_select:', calls['select'])
			print('\treproduce:', calls['reproduce'])
			print('\tmutate:', calls['mutate'])

	def conflicts(ind):
		'''Given a coloring of the graph, returns how many conflicts exist in the coloring'''
		calls['conflicts'] += 1
		return sum((1 for i in range(n) for j in range(i) if e[i][j] and ind[i] == ind[j]))

	def tournament_select():
		'''selects the strongest of the tournament with probability tournChance, 2nd strongest with 
		prob tournChance(1-tournChance), etc.'''
		calls['select'] += 1

		subset = randsample(population, tournSize) # sample the population
		subset.sort(key=lambda e: fitnesses[e]) # sort by fitness
		x = random() # get a random number

		for i in range(tournSize):
			p = tournChance*pow(1 - tournChance, i) # probibility of ith item being chosen
			if x < p:
				return subset[i]
			else:
				x -= p

		return subset[-1]

	def reproduce(parent1, parent2):
		'''returns a random child from parents using one-point crossover at a random split'''
		calls['reproduce'] += 1

		split = randrange(1, n)
		swap = randrange(2)
		if swap == 0:
			child = parent1[:split] + parent2[split:]
		else:
			child = parent2[:split] + parent1[split:]
		if OUTPUT: print('{} + {} = {}'.format(parent1, parent2, child))
		return child

	def mutate(ind):
		'''Randomly mutates the given coloring by changing a single node color'''
		calls['mutate'] += 1

		new = list(ind) # clone tuple into list so we can modify
		i = randrange(n) # random index in list
		new[i] = randrange(k) # set to random color
		return tuple(new) # cast back to tuple

	# ==============================================================================================
	if OUTPUT: print('n = {} k = {} maxIters = {} popSize = {} mutateChance = {} tournSize = {} tournChance = {}'.format(n, k, maxIters, popSize, mutateChance, tournSize, tournChance))

	# generate the population
	population = [tuple(randrange(k) for i in range(n)) for j in range(popSize)]
	fitnesses = {} # used to cache fitnesses to avoid excess calls to conflicts function

	for gen in range(maxIters):
		if OUTPUT: print('\nGeneration {}:'.format(gen))

		# update fitness cache
		fitnesses.update({ind: conflicts(ind) for ind in set(population) if ind not in fitnesses})

		# Calculate the best fitness this gen
		bestFit = 100000
		for individual in population:
			fit = fitnesses[individual]
			if OUTPUT: print('Individual: {} Conflicts: {}'.format(individual, fit))
			if fit == 0: # if we've found a perfect individual, done
				print_calls()
				return (True, gen, calls, individual, 0)
			if fit < bestFit:
				bestFit = fit

		if OUTPUT: print('Best: {}'.format(bestFit))

		# make the next generation
		# almost directly adapted from book, look there for explanations
		newPopulation = []
		for i in range(popSize):
			x = tournament_select()
			y = tournament_select()
			child = reproduce(x, y)
			mc  = mutateChance
			while mc > 0: # greater than 1 mutate chance makes multiple mutations
				if random() < mc:
					child = mutate(child)
				mc -= 1.0
			newPopulation.append(child)
		population = newPopulation

	fitnesses.update({ind: conflicts(ind) for ind in set(population) if ind not in fitnesses})
	best = min(population, key=lambda e: fitnesses[e])
	print_calls()
	return (False, gen, calls, best, fitnesses[best])