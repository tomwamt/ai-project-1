package Backtracking;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Backtracking {

	int MATRIXSIZE;
	int[][] adMatrix;
	int colorNum;
	Node[] Map;
	int totalDecisions;
	PrintWriter writer;

	public Backtracking() {
		String filename = "ten";
		colorNum = 4;
		adMatrix = new int[20][20];
		totalDecisions = 0;
		Scanner scan;
		try {
			scan = new Scanner(new File(filename));
			MATRIXSIZE = Integer.parseInt(scan.nextLine());
			Map = new Node[MATRIXSIZE];
			for (int i = 0; i < MATRIXSIZE; i++) 
			{
				double x = scan.nextDouble();
				double y = scan.nextDouble();
				Node newNode = new Node(x, y, colorNum, i);
				Map[i] = newNode;
			}
			for (int j = 0; j < MATRIXSIZE; j++) 
			{
				int[] adj = new int[MATRIXSIZE];
				for (int q = 0; q < MATRIXSIZE; q++) 
				{
					adj[q] = scan.nextInt();
				}
				Map[j].setAdj(adj);
			}
			// print();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try{
			
			
			writer = new PrintWriter(filename + "Output", "UTF-8");
			writer.println("Each algorithm was run a total of 4 times each for each of the two possible color sets, for a total of 24 separate tests.\n\n");
			for (int i = 0; i < 4; i++) 
			{
				writer.println("Now running backTrackingSimple iteration = " + (i+1) + " with "+ colorNum +" colors\n");
				backTrackingSimple(Map[(int) (Math.random() * (Map.length))]);
				if(check())
				{
					writer.println("\nThe backTrackingSimple algorithm SUCCESSFULLY colored all of the nodes.\n");
				}
				else
				{
					writer.println("\nThe backTrackingSimple algorithm FAILED to color all of the nodes.\n");
				}
				writer.println("The algorithm made " + totalDecisions + " decisions during it's execution.\n");
				
				writer.println(print());
				resetMap();
				totalDecisions = 0;
				writer.println("-----------------------------------------------------------------------------------------------------------------------\n-----------------------------------------------------------------------------------------------------------------------\n-----------------------------------------------------------------------------------------------------------------------\n");
			}
			
			
			
			
			for (int i = 0; i < 4; i++) 
			{
				writer.println("Now running backTrackingWithForwardChecking iteration = " + (i+1) + " with "+ colorNum +" colors\n");
				backTrackingWithForwardChecking(Map[(int) (Math.random() * (Map.length))]);
				if(check())
				{
					writer.println("\nThe backTrackingWithForwardChecking algorithm SUCCESSFULLY colored all of the nodes.\n");
				}
				else
				{
					writer.println("\nThe backTrackingWithForwardChecking algorithm FAILED to color all of the nodes.\n");
				}
				writer.println("The algorithm made " + totalDecisions + " decisions during it's execution.\n");
				
				writer.println(print());
				resetMap();
				totalDecisions = 0;
				writer.println("-----------------------------------------------------------------------------------------------------------------------\n-----------------------------------------------------------------------------------------------------------------------\n-----------------------------------------------------------------------------------------------------------------------\n");
			}
			
			
			
			for (int i = 0; i < 4; i++) 
			{
				writer.println("Now running backTrackingWithConstraintPropagation iteration = " + (i+1) + " with "+ colorNum +" colors\n");
				backTrackingWithConstraintPropagation(Map[(int) (Math.random() * (Map.length))]);
				if(check())
				{
					writer.println("\nThe backTrackingWithConstraintPropagation algorithm SUCCESSFULLY colored all of the nodes.\n");
				}
				else
				{
					writer.println("\nThe backTrackingWithConstraintPropagation algorithm FAILED to color all of the nodes.\n");
				}
				writer.println("The algorithm made " + totalDecisions + " decisions during it's execution.\n");
				
				writer.println(print());
				resetMap();
				totalDecisions = 0;
				writer.println("-----------------------------------------------------------------------------------------------------------------------\n-----------------------------------------------------------------------------------------------------------------------\n-----------------------------------------------------------------------------------------------------------------------\n");
			}
			
			
			colorNum = 3;
			
			for (int i = 0; i < 4; i++) 
			{
				writer.println("Now running backTrackingSimple iteration = " + (i+1) + " with "+ colorNum +" colors\n");
				backTrackingSimple(Map[(int) (Math.random() * (Map.length))]);
				if(check())
				{
					writer.println("\nThe backTrackingSimple algorithm SUCCESSFULLY colored all of the nodes.\n");
				}
				else
				{
					writer.println("\nThe backTrackingSimple algorithm FAILED to color all of the nodes.\n");
				}
				writer.println("The algorithm made " + totalDecisions + " decisions during it's execution.\n");
				
				writer.println(print());
				resetMap();
				totalDecisions = 0;
				writer.println("-----------------------------------------------------------------------------------------------------------------------\n-----------------------------------------------------------------------------------------------------------------------\n-----------------------------------------------------------------------------------------------------------------------\n");
			}
			
			
			for (int i = 0; i < 4; i++) 
			{
				writer.println("Now running backTrackingWithForwardChecking iteration = " + (i+1) + " with "+ colorNum +" colors\n");
				backTrackingWithForwardChecking(Map[(int) (Math.random() * (Map.length))]);
				if(check())
				{
					writer.println("\nThe backTrackingWithForwardChecking algorithm SUCCESSFULLY colored all of the nodes.\n");
				}
				else
				{
					writer.println("\nThe backTrackingWithForwardChecking algorithm FAILED to color all of the nodes.\n");
				}
				writer.println("The algorithm made " + totalDecisions + " decisions during it's execution.\n");
				
				writer.println(print());
				resetMap();
				totalDecisions = 0;
				writer.println("-----------------------------------------------------------------------------------------------------------------------\n-----------------------------------------------------------------------------------------------------------------------\n-----------------------------------------------------------------------------------------------------------------------\n");
			}
			
			for(int i = 0; i < 4; i++) 
			{
				writer.println("Now running backTrackingWithConstraintPropagation iteration = " + (i+1) + " with "+ colorNum +" colors\n");
				backTrackingWithConstraintPropagation(Map[(int) (Math.random() * (Map.length))]);
				if(check())
				{
					writer.println("\nThe backTrackingWithConstraintPropagation algorithm SUCCESSFULLY colored all of the nodes.\n");
				}
				else
				{
					writer.println("\nThe backTrackingWithConstraintPropagation algorithm FAILED to color all of the nodes.\n");
				}
				writer.println("The algorithm made " + totalDecisions + " decisions during it's execution.\n");
				
				writer.println(print());
				resetMap();
				totalDecisions = 0;
				writer.println("-----------------------------------------------------------------------------------------------------------------------\n-----------------------------------------------------------------------------------------------------------------------\n-----------------------------------------------------------------------------------------------------------------------\n");
			}
			
			
			writer.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	
	
	
	
	public void resetMap()
	{
		for(Node n: Map)
		{
			n.reset();
		}
	}
	
	public String print() {
		String s = "";
		for (Node n : Map) {
			s += n.print() + "\n";
		}
		
		return s;

	}

	public boolean hasAdj(Node n)// returns true if node has an adjscent node
									// share a color
	{
		boolean result = false;
		for (int x = 0; x < MATRIXSIZE; x++) {
			if (n.isAdj(x)) {
				if (n.getColor() == Map[x].getColor()) {
					// System.out.println("ADJ " + n.getIndex() +
					// " SHARES COLOR with " + x + "   " + n.getIndex() + "=" +
					// n.getColor() + "  " + x + "=" + Map[x].getColor());
					result = true;
				}
			}
		}
		return result;

	}

	public boolean allDone() {
		boolean done = true;
		for (Node n : Map) {
			if (n.getColor() == -1) {
				done = false;
			}
		}
		return done;

	}

	public Node newNode() {
		int index = (int) (Math.random() * (Map.length));
		while (Map[index % Map.length].getColor() != -1) {
			index++;
		}
		return Map[index % Map.length];
	}

	public boolean check() {
		for (Node n : Map) {
			if (hasAdj(n) || n.getColor() == -1) {
				return false;
			}
		}
		return true;
	}

	public boolean totalRestrictions(Node node) {
		int[] inds = node.getAdjInds();
		for (int ind : inds) {
			if (!Map[ind].testAvail(node.getColor())) {
				// System.out.println("TOTAL RESTRICTION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
				return false;
			}
		}
		return true;
	}

	public void restrict(Node n) {
		int[] inds = n.getAdjInds();
		for (int i : inds) {
			totalDecisions++;
			Map[i].restrict(n.getColor());
			if(MATRIXSIZE < 15)
				writer.println("Node " + i + " has been restricted from using color " + n.getColor() + " by node " + n.getIndex());
		}
	}
	
	public void resetRestrictions(Node node)
	{
		for(int i: node.getAdjInds())
		{
			if(Map[i].getColor() == -1)
			{
				Map[i].restrictReset();
				for(int ind: Map[i].getAdjInds())
				{
					if(Map[ind].getColor() != -1)
					{
						Map[i].restrict(Map[ind].getColor());
					}
				}
			}
		}
	}

	public int numConstraints(Node node, int col) {
		int num = 0;
		for (int ind : node.getAdjInds()) {
			if (Map[ind].doesConstraint(col)) {
				num++;
			}
		}
		return num;
	}

	public int[] bubbleSort(int[] arr, int[] cols) {
		int[] array = arr;
		for (int i = 0; i < array.length; i++) {
			for (int j = array.length - 1; j > i; j--) {
				if (array[j] < array[j - 1]) {
					int temp = array[j - 1];
					array[j - 1] = array[j];
					array[j] = temp;
					int temp2 = cols[j - 1];
					cols[j - 1] = cols[j];
					cols[j] = temp2;
				}
			}
		}
		return array;
	}

	public boolean backTrackingWithConstraintPropagation(Node node) {
		int[] colConstrains = new int[colorNum];
		int[] colorOrder = new int[colorNum];
		for (int k = 0; k < colorNum; k++) {
			colorOrder[k] = k;
			colConstrains[k] = numConstraints(node, k);
		}
		colConstrains = bubbleSort(colConstrains, colorOrder);
		/*
		 * System.out.println(node.getIndex() + ""); for(int o = 0; o <
		 * colorNum; o++) { System.out.println("color = " + colorOrder[o] +
		 * "  constraints = " + colConstrains[o]); }
		 */

		for (int i = 0; i < colorNum; i++) {
			node.setColor(colorOrder[i]);
			if(MATRIXSIZE < 15){
			writer.println("Color of node = " + node.getIndex() + " changed to = " + node.getColor());}
			totalDecisions++;
			if (!hasAdj(node) && totalRestrictions(node)) {
				// node.setColor(-1);
				restrict(node);

				if (allDone()) {
					// System.out.println("alldone");
					return true;
				}
				if (backTrackingWithConstraintPropagation(newNode())) {
					// System.out.println("true");
					return true;
				}
			}
		}
		node.reset();
		resetRestrictions(node);
		if(MATRIXSIZE < 15){
		writer.println("All colors of node = " + node.getIndex() + " tried, now backtracking...");}
		totalDecisions++;
		return false;
	}

	public boolean backTrackingWithForwardChecking(Node node) {
		int col = (int) (Math.random() * colorNum);
		for (int i = 0; i < colorNum; i++) {
			node.setColor((col + i) % colorNum);
			totalDecisions++;
			if(MATRIXSIZE < 15){
			writer.println("Color of node = " + node.getIndex() + " changed to = " + node.getColor());}
			if (!hasAdj(node) && totalRestrictions(node)) {
				// node.setColor(-1);
				restrict(node);

				if (allDone()) {
					System.out.println("alldone");
					return true;
				}
				if (backTrackingWithForwardChecking(newNode())) {
					System.out.println("true");
					return true;
				}
			}
		}
		totalDecisions++;
		if(MATRIXSIZE < 15){
		writer.println("All colors of node = " + node.getIndex() + " tried, now backtracking...");}
		node.reset();
		resetRestrictions(node);
		return false;
	}

	public boolean backTrackingSimple(Node node) {
		int col = (int) (Math.random() * colorNum);
		for (int i = 0; i < colorNum; i++) {
			node.setColor((col + i) % colorNum);
			if(MATRIXSIZE < 15){
			writer.println("Color of node = " + node.getIndex() + " changed to = " + node.getColor());}
			totalDecisions++;
			if (!hasAdj(node)) {
				// node.setColor(-1);

				if (allDone()) {
					System.out.println("alldone");
					return true;
				}
				if (backTrackingSimple(newNode())) {
					System.out.println("true");
					return true;
				}
			}
		}
		totalDecisions++;
		if(MATRIXSIZE < 15){
		writer.println("All colors of node = " + node.getIndex() + " tried, now backtracking...");}
		node.reset();
		return false;
	}

}
