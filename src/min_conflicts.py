'''		Global Variables	 '''
global verteces
global edges
global colors
global choiceLimit
global colorList
global decisions

''' 	Imports 	'''
import random
import problem_gen
import sys

def main():
	global verteces
	global edges
	global decisions
	
	graphName = sys.argv[1]
	print('Loading graph', graphName)
	verteces, edges = problem_gen.load_graph("..\\data\\" + graphName + ".txt")
	
	global colors # number of colors
	colors = int(sys.argv[2])
	
	global choiceLimit
	choiceLimit = int(sys.argv[3])
	
	randomize() # initialize the colorList
	print('Initial colors list:', colorList)
	decisions = 0
	k = 0
	# Loop until it's solved or time runs out
	while k < choiceLimit and perf_check() == False:
		solve_conflicts(random.randint(0,len(verteces)-1))
		k+=1

	print("We swapped colors " + str(decisions) + " times.")
	print('Final color list:', colorList)
	print('Success:', perf_check())
	problem_gen.draw_colored_graph(verteces, edges, colorList, ("Min_Conflicts_"+graphName+".png"))

def randomize():
	global verteces
	global edges
	global colors
	global choiceLimit
	global colorList

	# assign every node a random color
	nodeCount = len(verteces)
	colorList = []
	for i in range(0, nodeCount):
		newColor = random.randint(0,colors-1)
		colorList.append(newColor)

def solve_conflicts(node):
	global colors
	global colorList
	global verteces
	global edges
	global decisions

	nodeCount = len(verteces)
	colorCount = [0]*colors
	mins = [] # list because there could be ties
	leastCount = 9999999

	# count up neighboring colors
	for i in range(0, nodeCount):
		if edges[node][i]:
			colorCount[colorList[i]] += 1

	print('color counts around node '+str(node)+': '+str(colorCount))

	# if current node is conflicted
	if colorCount[colorList[node]] != 0:
		# loop through and find the mins
		for j in range(0,len(colorCount)):
			if colorCount[j] < leastCount:
				mins = [j]
				leastCount = colorCount[j]
			elif colorCount[j] == leastCount:
				mins.append(j)

		# randomly pick from the mins and assign it
		newColor = random.choice(mins)
		colorList[node] = newColor

		print("Switched node " + str(node) + " to color " + str(newColor) +".")
		decisions += 1

def perf_check():
	global colorList
	global edges
	global verteces
	global colorList

	# Loop through the edges and see if two adjacent nodes share a color
	nodeCount = len(verteces)
	for i in range(0,nodeCount):
		for j in range(0, i):
			if edges[i][j] and colorList[i] == colorList[j]:
				return False # conflict found

	return True

if __name__ == '__main__':
	main()