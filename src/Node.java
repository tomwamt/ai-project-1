package Backtracking;

public class Node {
	private double xCoor;
	private double yCoor;
	private int[] adMatrix;
	private int color;  // 0 = RED, 1 = GREEN, 2 = BLUE, 3 = YELLOW, -1 = NOCOLOR
	private int[] posCol;
	private int index;
	private int[] adjNodesInds;
	
	
	public Node(double x, double y, int ColorNum, int i)
	{
		color = -1;
		xCoor = x;
		yCoor = y;
		index = i;
		posCol = new int[ColorNum];
		for(int r = 0; r < posCol.length; r++)
		{
			posCol[r] = 1;
		}
	}
	
	public void setAdj(int[] adM)
	{
		adMatrix = adM;
		int size = 0;
		for(int k: adM)
		{
			if(k == 1)
				size++;
		}
		adjNodesInds = new int[size];
		int ind = 0;
		for(int i = 0; i < adMatrix.length; i++)
		{
			if(adMatrix[i] == 1)
			{
				adjNodesInds[ind] = i;
				ind++;
			}
		}
	}
	
	public int[] getAdjInds()
	{
		return adjNodesInds;
	}
	
	public void restrict(int x)
	{
		posCol[x] = 0;
		
	}
	
	public void restrictReset()
	{
		for(int r = 0; r < posCol.length; r++)
		{
			posCol[r] = 1;
		}
	}
	
	
	public boolean doesConstraint(int x)
	{
		if(color == -1)
		{
			if(posCol[x] == 1)
			{
				return true;
			}
			else
				return false;
		}
		return false;
	}
	
	
	
	
	/**
	 * 
	 * @param COLOR
	 * @return true if no total restriction, false if totally resticted. 
	 */
	public boolean testAvail(int COLOR)
	{
		if(hasAvail())
		{
			if(posCol[COLOR] == 1)
			{
				int z = 0;
				for(int i: posCol)
				{
					z += i;
				}
				if(z > 1)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}
	
	public int getIndex()
	{
		return index;
	}
	
	
	public String print()
	{
		String val = "";
		for(int i: adMatrix)
		{
			val += i + " ";
		}
		val += "   "+ index +"   " + xCoor + " " + yCoor + " COLOR = " + color;
		return val;
	}
	
	
	public void setColor(int l)
	{
		color = l;// 0 = RED, 1 = GREEN, 2 = BLUE, 3 = YELLOW
		//System.out.println(index + " assigned " + color);
		for(int i: posCol)
		{
			i = 0;
		}
		//posCol[l] = 0;
	}
	
	public boolean isAdj(int index)
	{
		if(adMatrix[index] == 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public int getColor(){
		return color;
	}
	
	public void reset()
	{
		//System.out.println("reset");
		color = -1;
		
		for(int r = 0; r < posCol.length; r++)
		{
			posCol[r] = 1;
		}
	}
	
	public boolean hasAvail() //returns true if has availible
	{
		//boolean hasAvail = false;
		for(int x = 0;x < posCol.length; x++)
		{
			if(posCol[x] == 1)
				return true;
		}
		return false;
	}
}
