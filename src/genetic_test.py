import problem_gen
import genetic
import time
from multiprocessing import Pool
from traceback import print_exc

class TestRun():
	pass

def main(args):
	test_sequence = []
	with open('genetic_test_params.csv', 'r') as f:
		for line in f.readlines():
			if line.isspace() or line[0] == '#':
				continue

			params = [p.strip() for p in line.split(',')]
			test = TestRun()
			test.name = params[0]
			test.filename = params[1]
			test.numRuns = int(params[2])
			test.colors = int(params[3])
			test.maxGens = int(params[4])
			test.popSize = int(params[5])
			test.mutateChance = float(params[6])
			test.tournSize = int(params[7])
			test.tournChance = float(params[8])
			test.results = None
			test_sequence.append(test)

	start = 0
	if len(args) > 0:
		start = int(args[0])

	end = len(test_sequence)+1
	if len(args) > 1:
		end = int(args[1]) + 1

	for test in test_sequence[start:end]:
		print('starting test', test.name)
		try:
			run_test(test)
			time.sleep(0.05)
			write_test(test)
			time.sleep(0.05)
		except Exception:
			print('exception occured, wrote details to file')
			with open('../tests/genetic/'+test.name+'.txt', 'w') as errf:
				print_exc(file=errf)

	for i in range(10, 80, 10):
		labels = ['10', '20', '30', '40', '50']
		j = (i-10)//2
		data = [[r[0]*test.popSize for r in test.results] for test in test_sequence[j:j+5]]
		dataSet = zip(labels, data)
		plotly_tests(dataSet, 'Population Size', 'Relative Runtime', 'Population Size vs. Relative Runtime for 4-coloring a {} vertex graph'.format(i), 'g{}pop.html'.format(i))

	for i in range(10, 80, 10):
		labels = ['5', '10', '15', '20', '25']
		j = (i-10)//2 + 35
		data = [[r[0]*test.popSize for r in test.results] for test in test_sequence[j:j+5]]
		dataSet = zip(labels, data)
		plotly_tests(dataSet, 'Tournament Size', 'Relative Runtime', 'Tournament Size vs. Relative Runtime for 4-coloring a {} vertex graph'.format(i), 'g{}tsize.html'.format(i))

	for i in range(10, 80, 10):
		labels = ['20%', '40%', '60%', '80%', '100%']
		j = (i-10)//2 + 70
		data = [[r[0]*test.popSize for r in test.results] for test in test_sequence[j:j+5]]
		dataSet = zip(labels, data)
		plotly_tests(dataSet, 'Tournament Chance', 'Relative Runtime', 'Tournament Chance vs. Relative Runtime for 4-coloring a {} vertex graph'.format(i), 'g{}tchance.html'.format(i))

	for i in range(10, 80, 10):
		labels = ['20%', '40%', '60%', '80%', '100%']
		j = (i-10)//2 + 105
		data = [[r[0]*test.popSize for r in test.results] for test in test_sequence[j:j+5]]
		dataSet = zip(labels, data)
		plotly_tests(dataSet, 'Mutation Chance', 'Relative Runtime', 'Mutation Chance vs. Relative Runtime for 4-coloring a {} vertex graph'.format(i), 'g{}mutate.html'.format(i))

def run_test(test):
	v, e = problem_gen.load_graph('../data/'+test.filename)
	n = len(v)

	args = (n, e, test.colors, test.maxGens, test.popSize, test.mutateChance, test.tournSize, test.tournChance)

	if test.numRuns == 1:
		test.results = [run(args)]
	else:
		numProcs = 4
		pool = Pool(numProcs)
		test.results = pool.map(run, constant_generator(args, test.numRuns), max(1, int(test.numRuns/(4*numProcs))))

	return test

def write_test(test):
	n = len(test.results)
	mini = 99999999
	maxi = 0
	totali = 0
	totals = 0
	minc = 999999999
	maxc = 0
	totalc = 0
	for success, iters, calls, colors, conflicts in test.results:
		if success:
			totals += 1

			if iters < mini:
				mini = iters
			if iters > maxi:
				maxi = iters
			totali += iters
		else:
			if conflicts < minc:
				minc = conflicts
			if conflicts > maxc:
				maxc = conflicts
			totalc += conflicts

	avgs = totals/n
	avgi = maxi
	if totals != 0:
		avgi = totali/totals
	avgc = 0
	if totals != n:
		avgc = totalc/(n - totals)

	f = open('../tests/genetic/'+test.name+'.txt', 'w')
	f.write('Test Parameters:\n')
	f.write('\tfilename: {}\n'.format(test.filename))
	f.write('\tnumRuns: {}\n'.format(test.numRuns))
	f.write('\tcolors: {}\n'.format(test.colors))
	f.write('\tmax generations: {}\n'.format(test.maxGens))
	f.write('\tpopulation size: {}\n'.format(test.popSize))
	f.write('\tmutation chance: {}\n'.format(test.mutateChance))
	f.write('\ttournament size: {}\n'.format(test.tournSize))
	f.write('\ttournament chance: {}\n'.format(test.tournChance))
	f.write('\nSummary:\n')
	f.write('{} tests were run. {:.1%} of them succeeded.\n'.format(n, avgs))
	f.write('Of the successes, the average number iterations was {:.2f} (min {}, max {}).\n'.format(avgi, mini, maxi))
	f.write('Of the failures, the average conflicts in the best result was {:.2f} (min {}, max {})\n'.format(avgc, minc, maxc))
	f.write('\nAll results:\n')
	f.write('Success  Iterations  Conflicts  Color Sequence\n')
	for success, iters, calls, colors, conflicts in test.results:
		f.write('{!s:>7}  {:>10d}  {:>9d}  {}\n'.format(success, iters, conflicts, colors))
	f.close()

def plotly_tests(dataSet, xAxis, yAxis, title, filename):
	data = []

	for label, data in dataSet:
		trace = graphs.Box(
			y=data,
			name=label
		)
		data.append(trace)

	layout = graphs.Layout(title=title)
	layout.xaxis.title = xAxis
	layour.yaxis.title = yAxis
	fig = graphs.Figure(data=data, layout=layout)

	offline.plot(data, filename=filename, auto_open=False)
	#
	#plotly.image.save_as(fig, 'my_plot.png')

def run(vals):
	return genetic.genetic(*vals)

def constant_generator(value, count):
	while count != 0:
		count -= 1
		yield value

if __name__ == '__main__':
	import sys
	from plotly import graph_objs as graphs, offline
	main(sys.argv[1:])