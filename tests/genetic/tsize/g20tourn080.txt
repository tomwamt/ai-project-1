Test Parameters:
	filename: graph20.txt
	numRuns: 300
	colors: 4
	max generations: 4000
	population size: 25
	mutation chance: 0.8
	tournament size: 20
	tournament chance: 0.5

Summary:
300 tests were run. 100.0% of them succeeded.
Of the successes, the average number iterations was 56.02 (min 6, max 667).
Of the failures, the average conflicts in the best result was 0.00 (min 999999999, max 0)

All results:
Success  Iterations  Conflicts  Color Sequence
   True          98          0  (2, 3, 1, 3, 1, 3, 1, 0, 3, 2, 3, 2, 0, 2, 3, 3, 1, 2, 3, 0)
   True          28          0  (3, 2, 0, 2, 3, 1, 2, 1, 3, 0, 2, 0, 2, 0, 3, 3, 1, 0, 1, 2)
   True           8          0  (3, 0, 1, 0, 3, 2, 0, 2, 3, 1, 0, 1, 0, 1, 3, 0, 2, 1, 2, 0)
   True          87          0  (1, 2, 3, 2, 1, 2, 3, 0, 2, 1, 3, 1, 3, 1, 2, 3, 0, 2, 3, 1)
   True          29          0  (0, 3, 1, 3, 0, 2, 3, 2, 1, 3, 1, 0, 3, 0, 1, 1, 0, 3, 1, 2)
   True         158          0  (0, 3, 2, 3, 0, 2, 3, 1, 2, 0, 3, 0, 3, 0, 2, 3, 1, 2, 1, 3)
   True          21          0  (1, 2, 3, 2, 1, 2, 3, 0, 2, 3, 2, 1, 0, 1, 2, 2, 1, 3, 2, 0)
   True          12          0  (2, 1, 3, 1, 2, 3, 1, 0, 2, 1, 3, 0, 1, 0, 2, 2, 3, 1, 3, 0)
   True          18          0  (1, 0, 2, 0, 2, 3, 0, 3, 1, 0, 0, 2, 0, 2, 1, 2, 3, 1, 3, 0)
   True          52          0  (0, 1, 2, 1, 0, 2, 1, 3, 2, 1, 2, 3, 0, 1, 2, 0, 3, 1, 2, 0)
   True           9          0  (2, 1, 3, 1, 2, 1, 3, 0, 2, 1, 3, 0, 3, 2, 1, 3, 0, 1, 3, 2)
   True          28          0  (3, 1, 0, 1, 3, 0, 1, 2, 3, 0, 1, 2, 1, 3, 0, 3, 2, 0, 2, 1)
   True          30          0  (1, 3, 2, 3, 1, 3, 2, 0, 3, 2, 3, 0, 2, 1, 3, 3, 1, 2, 3, 0)
   True          12          0  (1, 2, 3, 2, 3, 2, 3, 0, 2, 3, 2, 0, 1, 0, 3, 2, 0, 1, 0, 2)
   True          15          0  (2, 0, 1, 0, 1, 0, 1, 3, 0, 1, 0, 2, 1, 2, 0, 0, 3, 2, 0, 1)
   True          14          0  (3, 0, 2, 0, 2, 0, 2, 1, 0, 3, 0, 3, 2, 1, 3, 0, 1, 3, 1, 0)
   True          97          0  (2, 3, 0, 3, 0, 1, 0, 1, 2, 0, 3, 3, 0, 3, 2, 3, 1, 2, 1, 0)
   True          21          0  (0, 1, 2, 1, 2, 2, 1, 3, 2, 0, 1, 3, 0, 3, 1, 2, 3, 0, 1, 2)
   True          60          0  (3, 0, 1, 0, 1, 1, 0, 2, 1, 3, 0, 3, 2, 0, 3, 1, 0, 3, 0, 2)
   True         123          0  (3, 1, 0, 1, 3, 0, 1, 2, 3, 1, 1, 2, 1, 2, 0, 3, 2, 0, 2, 1)
   True          37          0  (2, 1, 3, 1, 2, 0, 3, 0, 2, 1, 3, 1, 3, 1, 2, 3, 0, 1, 0, 3)
   True         231          0  (2, 1, 3, 1, 3, 1, 3, 0, 2, 3, 1, 0, 3, 2, 1, 1, 0, 2, 1, 3)
   True          43          0  (1, 3, 2, 3, 1, 0, 3, 0, 2, 3, 3, 1, 3, 1, 2, 1, 0, 2, 0, 3)
   True           9          0  (3, 1, 0, 1, 0, 0, 1, 2, 3, 0, 3, 2, 1, 2, 3, 3, 2, 1, 3, 0)
   True          12          0  (1, 2, 3, 2, 1, 3, 2, 0, 3, 2, 3, 0, 2, 0, 1, 3, 0, 2, 0, 3)
   True          14          0  (1, 3, 2, 3, 1, 0, 2, 0, 1, 2, 3, 3, 2, 1, 0, 1, 3, 2, 0, 1)
   True          16          0  (2, 0, 1, 0, 2, 1, 0, 3, 1, 0, 1, 3, 0, 3, 1, 1, 2, 0, 1, 3)
   True           7          0  (2, 3, 0, 3, 0, 3, 0, 1, 2, 3, 2, 1, 0, 1, 3, 0, 1, 3, 1, 0)
   True          70          0  (2, 0, 1, 0, 2, 3, 1, 3, 0, 1, 0, 2, 1, 2, 3, 0, 2, 1, 3, 0)
   True          13          0  (2, 1, 0, 1, 2, 3, 0, 3, 2, 1, 0, 1, 0, 1, 2, 0, 3, 1, 3, 0)
   True          13          0  (3, 0, 1, 0, 3, 2, 0, 2, 1, 0, 0, 3, 0, 1, 3, 3, 2, 1, 2, 0)
   True          26          0  (0, 1, 3, 1, 0, 1, 3, 2, 1, 0, 3, 2, 3, 2, 1, 3, 2, 1, 2, 3)
   True           9          0  (3, 0, 2, 0, 2, 1, 2, 1, 0, 3, 0, 3, 2, 0, 3, 2, 1, 3, 1, 2)
   True          35          0  (0, 2, 3, 2, 3, 2, 3, 1, 2, 0, 2, 0, 1, 3, 0, 2, 3, 0, 1, 2)
   True          17          0  (2, 1, 3, 1, 3, 1, 3, 0, 1, 3, 1, 0, 3, 2, 1, 1, 0, 2, 1, 3)
   True          21          0  (3, 0, 1, 0, 1, 2, 1, 2, 3, 1, 0, 0, 1, 0, 3, 0, 2, 3, 2, 1)
   True         148          0  (1, 2, 3, 2, 3, 2, 3, 0, 2, 1, 2, 0, 1, 0, 3, 3, 0, 1, 0, 2)
   True          17          0  (1, 2, 3, 2, 1, 0, 2, 0, 1, 3, 3, 3, 2, 3, 1, 1, 0, 2, 0, 3)
   True          47          0  (1, 3, 0, 3, 1, 2, 0, 2, 1, 3, 0, 3, 0, 1, 3, 1, 2, 3, 0, 1)
   True          15          0  (3, 1, 2, 1, 3, 0, 1, 0, 2, 1, 2, 3, 1, 3, 2, 3, 0, 1, 2, 3)
   True          55          0  (3, 1, 2, 1, 2, 1, 2, 0, 1, 2, 3, 3, 2, 0, 1, 3, 0, 1, 0, 2)
   True          19          0  (1, 2, 0, 2, 1, 3, 2, 3, 1, 0, 2, 0, 2, 0, 1, 1, 3, 0, 3, 2)
   True          13          0  (3, 2, 1, 2, 1, 1, 2, 0, 1, 2, 3, 0, 2, 0, 1, 1, 0, 2, 0, 3)
   True          12          0  (2, 3, 0, 3, 0, 1, 0, 1, 2, 0, 2, 3, 0, 2, 3, 2, 1, 3, 1, 2)
   True          19          0  (2, 3, 0, 3, 2, 3, 0, 1, 3, 0, 3, 2, 1, 2, 0, 3, 2, 0, 1, 3)
   True          14          0  (3, 2, 1, 2, 1, 1, 2, 0, 3, 1, 2, 0, 2, 0, 3, 2, 0, 3, 0, 1)
   True          23          0  (3, 0, 1, 0, 3, 0, 1, 2, 0, 3, 1, 2, 3, 1, 0, 1, 2, 0, 2, 3)
   True          27          0  (1, 2, 3, 2, 3, 3, 2, 0, 3, 2, 1, 0, 1, 0, 2, 3, 0, 2, 1, 3)
   True          10          0  (0, 2, 3, 2, 3, 3, 2, 1, 3, 0, 2, 1, 0, 2, 3, 3, 1, 0, 1, 2)
   True          13          0  (2, 1, 0, 1, 2, 1, 0, 3, 2, 1, 1, 3, 0, 3, 1, 2, 3, 0, 1, 2)
   True          80          0  (3, 2, 1, 2, 1, 0, 1, 0, 3, 2, 2, 2, 1, 2, 3, 1, 0, 3, 2, 1)
   True          16          0  (1, 0, 3, 0, 3, 3, 0, 2, 1, 0, 0, 2, 0, 2, 1, 3, 2, 1, 2, 0)
   True         335          0  (1, 0, 2, 0, 1, 0, 2, 3, 0, 1, 2, 3, 1, 3, 2, 2, 3, 0, 2, 1)
   True          27          0  (0, 1, 3, 1, 0, 3, 1, 2, 3, 0, 1, 0, 1, 0, 3, 1, 2, 3, 2, 1)
   True           7          0  (0, 1, 3, 1, 0, 3, 1, 2, 3, 0, 3, 2, 0, 2, 1, 3, 2, 1, 3, 0)
   True          24          0  (0, 2, 1, 2, 1, 2, 1, 3, 2, 0, 0, 3, 1, 3, 2, 1, 3, 2, 0, 1)
   True          87          0  (0, 2, 3, 2, 0, 3, 2, 1, 3, 0, 2, 1, 0, 2, 3, 2, 1, 3, 2, 0)
   True          36          0  (3, 0, 2, 0, 2, 2, 0, 1, 2, 3, 0, 3, 1, 3, 2, 2, 0, 3, 0, 1)
   True          25          0  (3, 1, 2, 1, 2, 2, 1, 0, 2, 3, 1, 3, 0, 1, 2, 2, 1, 3, 1, 0)
   True          25          0  (2, 3, 0, 3, 0, 0, 3, 1, 0, 3, 2, 2, 1, 2, 3, 0, 2, 3, 2, 0)
   True          50          0  (2, 1, 3, 1, 3, 1, 3, 0, 1, 2, 1, 0, 2, 3, 1, 1, 3, 2, 1, 0)
   True          68          0  (2, 1, 3, 1, 3, 1, 3, 0, 2, 3, 2, 0, 3, 2, 1, 2, 0, 1, 0, 3)
   True         169          0  (1, 0, 3, 0, 1, 0, 3, 2, 0, 1, 0, 2, 3, 2, 0, 0, 2, 3, 2, 1)
   True          37          0  (3, 2, 0, 2, 0, 2, 0, 1, 2, 3, 2, 1, 3, 0, 1, 2, 0, 3, 1, 2)
   True         203          0  (2, 3, 0, 3, 2, 1, 3, 1, 2, 3, 0, 0, 3, 0, 1, 2, 0, 3, 1, 2)
   True          14          0  (1, 0, 3, 0, 3, 0, 3, 2, 0, 1, 0, 2, 1, 2, 0, 0, 3, 1, 0, 2)
   True          10          0  (3, 1, 0, 1, 0, 0, 1, 2, 0, 1, 3, 2, 3, 1, 0, 3, 2, 1, 2, 3)
   True         106          0  (1, 2, 0, 2, 1, 0, 2, 3, 0, 1, 0, 3, 1, 2, 0, 0, 3, 2, 0, 1)
   True         227          0  (1, 2, 0, 2, 0, 2, 0, 3, 2, 1, 2, 1, 3, 1, 2, 2, 0, 1, 2, 3)
   True          18          0  (3, 0, 2, 0, 3, 1, 0, 1, 2, 0, 0, 3, 0, 3, 2, 3, 1, 2, 1, 0)
   True          22          0  (0, 2, 1, 2, 1, 1, 2, 3, 1, 0, 0, 3, 0, 3, 2, 1, 3, 2, 3, 0)
   True           9          0  (2, 1, 3, 1, 3, 3, 1, 0, 3, 2, 1, 0, 1, 0, 2, 1, 0, 2, 1, 3)
   True          20          0  (2, 1, 3, 1, 2, 3, 1, 0, 3, 1, 3, 0, 2, 0, 1, 2, 0, 1, 0, 2)
   True          42          0  (3, 1, 2, 1, 2, 0, 1, 0, 2, 1, 3, 3, 1, 2, 0, 2, 3, 1, 0, 2)
   True          45          0  (3, 1, 2, 1, 3, 1, 2, 0, 3, 2, 2, 0, 2, 0, 3, 3, 0, 1, 0, 2)
   True          23          0  (3, 0, 2, 0, 2, 1, 0, 1, 2, 0, 3, 3, 0, 3, 2, 2, 1, 0, 1, 3)
   True          34          0  (2, 1, 0, 1, 2, 3, 1, 3, 2, 0, 1, 0, 1, 2, 0, 1, 3, 0, 3, 1)
   True          39          0  (2, 1, 3, 1, 3, 3, 1, 0, 3, 1, 2, 0, 2, 1, 3, 2, 0, 1, 0, 2)
   True          82          0  (3, 1, 2, 1, 3, 0, 2, 0, 3, 1, 2, 1, 2, 1, 3, 3, 0, 1, 0, 2)
   True          13          0  (2, 0, 1, 0, 1, 1, 0, 3, 1, 2, 2, 3, 2, 3, 1, 1, 3, 0, 3, 2)
   True          11          0  (0, 1, 2, 1, 0, 2, 1, 3, 2, 1, 2, 3, 1, 3, 2, 0, 3, 1, 3, 0)
   True         292          0  (0, 1, 3, 1, 0, 3, 1, 2, 0, 3, 1, 2, 1, 2, 3, 1, 2, 3, 1, 0)
   True         177          0  (0, 1, 2, 1, 2, 2, 1, 3, 2, 0, 1, 0, 3, 1, 0, 2, 1, 0, 1, 3)
   True         134          0  (2, 1, 3, 1, 3, 1, 3, 0, 1, 2, 1, 2, 0, 3, 2, 1, 3, 2, 0, 1)
   True          57          0  (0, 2, 3, 2, 3, 1, 2, 1, 0, 2, 0, 3, 2, 3, 0, 0, 1, 2, 0, 3)
   True           7          0  (0, 1, 2, 1, 0, 1, 2, 3, 0, 1, 2, 3, 2, 3, 1, 0, 3, 1, 3, 0)
   True          29          0  (3, 0, 2, 0, 3, 2, 0, 1, 2, 3, 2, 1, 3, 1, 0, 2, 1, 0, 1, 2)
   True           9          0  (2, 3, 0, 3, 0, 0, 3, 1, 0, 3, 3, 1, 3, 1, 2, 0, 1, 2, 1, 0)
   True          17          0  (1, 3, 2, 3, 2, 2, 3, 0, 1, 2, 1, 0, 3, 1, 2, 1, 0, 3, 0, 1)
   True         158          0  (3, 2, 1, 2, 1, 2, 1, 0, 2, 3, 2, 0, 1, 0, 3, 2, 0, 3, 0, 1)
   True          95          0  (3, 2, 0, 2, 0, 0, 2, 1, 0, 3, 2, 1, 2, 1, 0, 0, 1, 3, 1, 2)
   True          26          0  (2, 3, 1, 3, 1, 3, 1, 0, 3, 1, 2, 2, 1, 0, 2, 2, 0, 3, 2, 1)
   True           8          0  (1, 0, 2, 0, 1, 0, 2, 3, 0, 2, 0, 3, 1, 2, 0, 0, 3, 2, 3, 1)
   True          47          0  (2, 1, 0, 1, 2, 0, 1, 3, 0, 1, 0, 3, 2, 3, 1, 2, 3, 1, 3, 0)
   True         132          0  (2, 1, 0, 1, 0, 3, 0, 3, 1, 0, 2, 2, 0, 2, 1, 2, 3, 1, 3, 0)
   True          24          0  (2, 0, 3, 0, 3, 0, 3, 1, 0, 2, 0, 2, 1, 3, 2, 0, 3, 2, 1, 0)
   True          94          0  (0, 3, 1, 3, 1, 2, 1, 2, 3, 1, 3, 0, 1, 3, 0, 3, 2, 0, 2, 3)
   True         225          0  (3, 1, 2, 1, 2, 0, 1, 0, 3, 1, 3, 2, 1, 2, 0, 3, 2, 1, 0, 3)
   True          52          0  (2, 3, 0, 3, 0, 3, 0, 1, 3, 0, 2, 2, 0, 1, 3, 2, 1, 3, 2, 0)
   True           9          0  (0, 3, 2, 3, 0, 1, 2, 1, 3, 2, 3, 0, 2, 0, 1, 3, 0, 2, 1, 3)
   True          27          0  (1, 2, 0, 2, 0, 2, 0, 3, 1, 2, 1, 3, 0, 3, 2, 0, 3, 2, 1, 0)
   True           8          0  (0, 1, 3, 1, 0, 1, 3, 2, 0, 1, 1, 2, 3, 2, 1, 0, 2, 3, 1, 0)
   True          15          0  (0, 3, 2, 3, 0, 2, 3, 1, 0, 3, 2, 1, 3, 1, 0, 2, 1, 3, 1, 2)
   True          38          0  (1, 0, 2, 0, 2, 2, 0, 3, 2, 0, 1, 1, 3, 1, 0, 2, 1, 0, 3, 2)
   True          77          0  (2, 0, 3, 0, 3, 3, 0, 1, 3, 2, 0, 2, 1, 0, 2, 3, 0, 2, 0, 3)
   True         131          0  (1, 3, 2, 3, 2, 2, 3, 0, 2, 1, 3, 1, 0, 3, 1, 2, 3, 1, 3, 0)
   True           7          0  (0, 3, 1, 3, 0, 1, 3, 2, 1, 3, 1, 0, 3, 2, 0, 1, 2, 3, 2, 1)
   True          40          0  (1, 2, 3, 2, 1, 2, 3, 0, 2, 1, 3, 0, 3, 1, 2, 3, 0, 2, 3, 1)
   True          34          0  (2, 1, 3, 1, 3, 3, 1, 0, 3, 2, 1, 2, 1, 0, 2, 1, 0, 2, 0, 3)
   True           6          0  (1, 2, 3, 2, 3, 2, 3, 0, 1, 2, 1, 0, 3, 0, 2, 1, 0, 2, 1, 3)
   True          35          0  (1, 0, 3, 0, 1, 0, 3, 2, 0, 3, 0, 2, 3, 1, 0, 1, 2, 3, 2, 1)
   True          84          0  (0, 2, 1, 2, 0, 3, 2, 3, 1, 0, 2, 0, 2, 0, 1, 2, 3, 1, 3, 2)
   True          17          0  (3, 1, 2, 1, 2, 2, 1, 0, 2, 1, 3, 0, 1, 0, 2, 2, 3, 1, 3, 0)
   True          10          0  (3, 2, 1, 2, 3, 0, 2, 0, 1, 2, 1, 3, 2, 3, 0, 1, 3, 2, 0, 1)
   True         243          0  (0, 3, 2, 3, 2, 2, 3, 1, 2, 0, 3, 1, 0, 1, 3, 3, 1, 0, 1, 2)
   True          13          0  (3, 0, 2, 0, 2, 0, 2, 1, 0, 3, 0, 1, 3, 1, 2, 0, 1, 3, 1, 0)
   True          18          0  (1, 3, 0, 3, 0, 3, 0, 2, 1, 0, 3, 2, 0, 1, 3, 3, 2, 1, 2, 0)
   True          18          0  (3, 1, 2, 1, 2, 0, 1, 0, 2, 1, 3, 3, 1, 3, 0, 2, 3, 1, 0, 2)
   True          82          0  (3, 1, 2, 1, 3, 1, 2, 0, 1, 2, 1, 0, 3, 0, 2, 1, 0, 2, 0, 3)
   True         102          0  (3, 1, 0, 1, 0, 1, 0, 2, 3, 1, 1, 2, 0, 3, 1, 0, 2, 3, 2, 0)
   True         161          0  (3, 0, 1, 0, 1, 0, 1, 2, 0, 1, 0, 2, 1, 2, 3, 0, 2, 3, 2, 1)
   True          15          0  (2, 1, 3, 1, 2, 0, 1, 0, 3, 1, 1, 2, 1, 2, 3, 2, 0, 3, 0, 2)
   True          16          0  (0, 2, 1, 2, 1, 2, 1, 3, 2, 0, 2, 3, 0, 1, 2, 2, 3, 0, 2, 1)
   True          12          0  (3, 1, 2, 1, 2, 1, 2, 0, 3, 2, 1, 0, 2, 3, 1, 1, 0, 3, 1, 2)
   True          52          0  (2, 1, 0, 1, 2, 0, 1, 3, 0, 1, 0, 2, 3, 2, 0, 0, 2, 1, 0, 3)
   True          49          0  (3, 1, 0, 1, 3, 1, 0, 2, 3, 0, 0, 2, 0, 2, 3, 3, 2, 1, 2, 0)
   True          69          0  (3, 2, 0, 2, 0, 0, 2, 1, 0, 3, 2, 3, 1, 2, 0, 0, 2, 3, 2, 1)
   True          19          0  (0, 1, 2, 1, 0, 2, 1, 3, 2, 1, 2, 0, 1, 0, 2, 2, 3, 1, 2, 0)
   True          12          0  (2, 0, 3, 0, 3, 3, 0, 1, 3, 0, 2, 2, 1, 2, 0, 3, 2, 0, 2, 1)
   True          90          0  (3, 0, 1, 0, 1, 1, 0, 2, 1, 3, 0, 3, 2, 3, 1, 1, 0, 3, 0, 2)
   True          23          0  (0, 1, 2, 1, 0, 1, 2, 3, 1, 0, 2, 3, 2, 0, 1, 2, 3, 1, 2, 0)
   True          17          0  (0, 2, 1, 2, 0, 1, 2, 3, 0, 1, 1, 3, 2, 0, 1, 0, 3, 2, 3, 0)
   True          16          0  (0, 3, 2, 3, 2, 2, 3, 1, 2, 3, 0, 1, 3, 0, 2, 0, 1, 3, 1, 0)
   True          86          0  (2, 0, 3, 0, 2, 0, 3, 1, 2, 3, 0, 1, 3, 1, 2, 2, 0, 3, 0, 1)
   True          15          0  (1, 3, 2, 3, 1, 0, 2, 0, 1, 3, 2, 3, 2, 3, 1, 1, 0, 3, 0, 2)
   True          17          0  (3, 2, 1, 2, 3, 1, 2, 0, 1, 3, 2, 0, 3, 2, 1, 2, 0, 1, 2, 3)
   True          30          0  (2, 0, 3, 0, 2, 3, 0, 1, 3, 2, 0, 2, 0, 1, 3, 0, 1, 3, 1, 0)
   True          38          0  (0, 2, 3, 2, 3, 1, 3, 1, 2, 0, 0, 0, 3, 0, 2, 3, 1, 2, 1, 3)
   True          10          0  (2, 1, 0, 1, 2, 3, 1, 3, 0, 2, 0, 2, 1, 2, 0, 0, 3, 1, 3, 2)
   True          34          0  (3, 2, 1, 2, 3, 0, 2, 0, 1, 2, 1, 3, 2, 1, 3, 1, 0, 2, 0, 1)
   True           8          0  (1, 2, 0, 2, 0, 0, 2, 3, 0, 1, 2, 3, 2, 1, 0, 0, 3, 1, 3, 2)
   True          20          0  (0, 1, 3, 1, 3, 3, 1, 2, 3, 0, 1, 2, 0, 2, 1, 1, 2, 0, 1, 3)
   True          18          0  (0, 3, 2, 3, 0, 3, 2, 1, 3, 2, 3, 0, 1, 0, 3, 3, 0, 2, 3, 1)
   True         330          0  (1, 3, 2, 3, 1, 2, 3, 0, 2, 3, 3, 1, 3, 1, 2, 1, 0, 2, 0, 3)
   True          15          0  (3, 2, 0, 2, 0, 0, 2, 1, 3, 0, 3, 1, 2, 1, 3, 3, 1, 2, 3, 0)
   True          47          0  (2, 1, 3, 1, 2, 3, 1, 0, 3, 1, 3, 2, 1, 0, 3, 3, 2, 1, 3, 0)
   True          18          0  (2, 1, 0, 1, 0, 1, 0, 3, 2, 0, 1, 3, 0, 3, 2, 1, 3, 2, 1, 0)
   True          19          0  (1, 0, 3, 0, 1, 3, 0, 2, 3, 0, 3, 2, 0, 2, 1, 3, 2, 0, 2, 3)
   True          37          0  (2, 0, 1, 0, 2, 3, 1, 3, 2, 1, 0, 0, 1, 0, 2, 2, 0, 1, 0, 3)
   True          48          0  (1, 3, 2, 3, 2, 0, 2, 0, 1, 2, 3, 3, 2, 3, 1, 3, 0, 1, 3, 2)
   True          31          0  (3, 2, 1, 2, 3, 0, 2, 0, 3, 2, 2, 1, 2, 3, 1, 3, 0, 1, 2, 3)
   True          75          0  (1, 0, 2, 0, 1, 3, 2, 3, 1, 0, 2, 0, 2, 1, 0, 1, 3, 0, 3, 1)
   True          24          0  (3, 1, 2, 1, 3, 2, 1, 0, 2, 1, 2, 3, 0, 1, 2, 2, 3, 1, 2, 0)
   True           8          0  (0, 1, 3, 1, 0, 3, 1, 2, 3, 1, 3, 2, 1, 2, 3, 3, 0, 1, 3, 2)
   True          39          0  (1, 0, 2, 0, 2, 2, 0, 3, 2, 0, 1, 3, 0, 3, 1, 1, 3, 0, 3, 2)
   True          10          0  (0, 2, 3, 2, 3, 3, 2, 1, 3, 0, 2, 1, 0, 1, 3, 3, 2, 0, 2, 1)
   True         210          0  (2, 3, 1, 3, 2, 1, 3, 0, 1, 3, 3, 0, 2, 3, 1, 2, 0, 1, 0, 2)
   True         667          0  (0, 1, 3, 1, 0, 3, 1, 2, 3, 1, 1, 0, 1, 2, 3, 0, 2, 3, 1, 0)
   True          96          0  (2, 3, 0, 3, 2, 1, 3, 1, 2, 0, 3, 0, 3, 2, 0, 3, 1, 0, 3, 2)
   True          27          0  (1, 0, 3, 0, 3, 3, 0, 2, 3, 0, 0, 2, 1, 2, 3, 3, 2, 1, 2, 0)
   True          30          0  (0, 3, 2, 3, 0, 3, 2, 1, 0, 2, 3, 1, 2, 0, 1, 0, 3, 2, 3, 0)
   True          20          0  (1, 3, 0, 3, 0, 3, 0, 2, 3, 0, 1, 2, 1, 2, 3, 1, 2, 3, 1, 0)
   True         104          0  (3, 1, 2, 1, 2, 1, 2, 0, 1, 3, 1, 3, 0, 3, 1, 1, 2, 3, 1, 0)
   True          13          0  (2, 3, 0, 3, 0, 1, 3, 1, 2, 3, 3, 0, 3, 2, 0, 0, 1, 2, 1, 3)
   True          43          0  (3, 2, 1, 2, 1, 2, 1, 0, 2, 1, 2, 3, 1, 3, 2, 2, 0, 3, 2, 1)
   True          13          0  (2, 1, 0, 1, 2, 0, 1, 3, 0, 2, 1, 3, 2, 1, 0, 1, 3, 0, 3, 1)
   True          36          0  (3, 0, 2, 0, 2, 0, 2, 1, 3, 2, 3, 1, 2, 3, 0, 3, 1, 0, 1, 2)
   True         110          0  (3, 0, 1, 0, 1, 0, 1, 2, 3, 1, 3, 2, 1, 2, 3, 3, 2, 0, 3, 1)
   True          19          0  (1, 2, 3, 2, 3, 3, 2, 0, 1, 3, 2, 0, 2, 1, 3, 2, 0, 1, 0, 2)
   True          39          0  (2, 3, 1, 3, 2, 0, 1, 0, 2, 3, 1, 3, 1, 2, 3, 2, 0, 3, 0, 2)
   True          10          0  (2, 1, 0, 1, 0, 1, 0, 3, 2, 1, 2, 3, 0, 3, 1, 2, 3, 1, 3, 2)
   True           9          0  (2, 0, 3, 0, 3, 3, 0, 1, 3, 0, 2, 1, 0, 1, 2, 2, 1, 0, 1, 3)
   True          14          0  (0, 1, 3, 1, 3, 3, 1, 2, 3, 0, 1, 2, 0, 1, 3, 3, 1, 0, 1, 2)
   True          81          0  (2, 0, 1, 0, 1, 3, 0, 3, 1, 0, 2, 2, 0, 1, 2, 2, 3, 0, 2, 1)
   True          20          0  (1, 0, 2, 0, 1, 0, 2, 3, 1, 0, 0, 3, 2, 3, 0, 1, 3, 2, 3, 1)
   True          22          0  (0, 3, 1, 3, 1, 2, 3, 2, 1, 3, 0, 0, 3, 1, 2, 1, 0, 3, 0, 1)
   True         112          0  (2, 3, 0, 3, 0, 3, 0, 1, 3, 0, 3, 1, 2, 1, 0, 3, 1, 2, 1, 3)
   True          30          0  (2, 1, 0, 1, 0, 3, 1, 3, 2, 1, 2, 0, 1, 0, 2, 2, 0, 1, 2, 3)
   True          62          0  (2, 1, 0, 1, 0, 3, 1, 3, 0, 1, 2, 2, 1, 0, 2, 2, 3, 1, 2, 0)
   True          25          0  (2, 1, 3, 1, 2, 3, 1, 0, 3, 1, 3, 0, 1, 0, 3, 3, 0, 1, 0, 2)
   True          10          0  (3, 1, 2, 1, 3, 1, 2, 0, 1, 2, 1, 3, 0, 3, 2, 1, 3, 2, 0, 1)
   True           7          0  (3, 2, 0, 2, 3, 2, 0, 1, 2, 0, 2, 3, 0, 1, 3, 2, 1, 0, 1, 2)
   True          13          0  (0, 2, 3, 2, 3, 2, 3, 1, 0, 2, 2, 1, 3, 1, 0, 3, 1, 0, 2, 3)
   True          55          0  (0, 1, 2, 1, 2, 1, 2, 3, 0, 2, 0, 3, 2, 0, 1, 0, 3, 1, 0, 2)
   True          17          0  (1, 2, 0, 2, 1, 0, 2, 3, 0, 2, 0, 3, 1, 3, 2, 0, 3, 2, 3, 0)
   True          37          0  (2, 0, 1, 0, 2, 3, 1, 3, 2, 0, 1, 0, 1, 2, 0, 2, 3, 0, 3, 1)
   True           9          0  (1, 0, 2, 0, 1, 2, 0, 3, 2, 0, 2, 3, 1, 0, 2, 1, 3, 0, 3, 1)
   True          21          0  (3, 2, 1, 2, 1, 0, 1, 0, 2, 3, 3, 3, 1, 2, 3, 1, 0, 2, 3, 1)
   True           8          0  (2, 0, 1, 0, 1, 0, 1, 3, 2, 1, 0, 3, 1, 2, 0, 0, 3, 2, 0, 1)
   True          20          0  (3, 2, 1, 2, 1, 1, 2, 0, 1, 3, 3, 0, 3, 0, 2, 1, 0, 2, 3, 1)
   True          13          0  (1, 3, 0, 3, 1, 0, 3, 2, 0, 3, 3, 2, 1, 3, 0, 1, 2, 0, 2, 3)
   True          60          0  (3, 1, 2, 1, 2, 2, 1, 0, 2, 3, 1, 3, 0, 1, 3, 2, 1, 3, 1, 0)
   True          15          0  (0, 2, 1, 2, 1, 2, 1, 3, 2, 1, 2, 0, 1, 0, 2, 2, 3, 0, 2, 1)
   True          16          0  (0, 2, 3, 2, 0, 1, 2, 1, 0, 2, 3, 3, 2, 3, 0, 0, 3, 2, 3, 1)
   True          11          0  (3, 0, 2, 0, 2, 0, 2, 1, 3, 2, 3, 1, 2, 1, 3, 3, 1, 0, 3, 2)
   True         301          0  (3, 2, 1, 2, 1, 0, 2, 0, 3, 1, 2, 1, 2, 1, 3, 2, 0, 3, 2, 1)
   True          14          0  (0, 3, 1, 3, 1, 1, 3, 2, 1, 0, 0, 0, 3, 2, 1, 1, 2, 3, 2, 0)
   True          55          0  (2, 3, 1, 3, 1, 1, 3, 0, 1, 3, 2, 0, 3, 2, 1, 1, 2, 3, 2, 0)
   True          16          0  (0, 1, 3, 1, 3, 3, 1, 2, 3, 1, 0, 2, 0, 2, 3, 3, 2, 1, 2, 0)
   True          14          0  (3, 2, 0, 2, 3, 0, 2, 1, 3, 2, 2, 1, 2, 1, 3, 3, 1, 0, 1, 2)
   True          16          0  (1, 2, 0, 2, 0, 0, 2, 3, 0, 1, 2, 3, 1, 2, 3, 0, 2, 1, 2, 0)
   True          10          0  (1, 0, 3, 0, 3, 2, 3, 2, 0, 3, 1, 1, 3, 0, 1, 1, 2, 0, 1, 3)
   True          59          0  (2, 1, 3, 1, 3, 3, 1, 0, 3, 2, 1, 2, 0, 2, 3, 3, 1, 2, 1, 0)
   True          17          0  (2, 3, 0, 3, 0, 0, 3, 1, 0, 2, 3, 1, 2, 3, 0, 3, 1, 2, 1, 3)
   True          23          0  (0, 3, 2, 3, 2, 3, 2, 1, 3, 0, 3, 1, 0, 2, 1, 3, 2, 0, 1, 3)
   True          43          0  (2, 0, 3, 0, 2, 3, 0, 1, 3, 0, 0, 1, 2, 0, 3, 2, 1, 3, 1, 0)
   True          36          0  (1, 0, 2, 0, 2, 0, 2, 3, 0, 1, 1, 1, 2, 1, 0, 2, 3, 0, 1, 2)
   True          59          0  (2, 1, 3, 1, 2, 3, 1, 0, 3, 1, 1, 0, 2, 1, 3, 2, 0, 3, 1, 2)
   True          10          0  (2, 1, 0, 1, 0, 3, 0, 3, 2, 0, 1, 1, 0, 2, 1, 1, 3, 2, 1, 0)
   True          77          0  (2, 0, 1, 0, 1, 1, 0, 3, 2, 0, 2, 3, 0, 2, 3, 2, 1, 0, 3, 2)
   True          77          0  (2, 1, 0, 1, 2, 3, 1, 3, 2, 0, 1, 0, 1, 2, 0, 1, 3, 0, 3, 2)
   True          71          0  (0, 3, 1, 3, 1, 2, 1, 2, 0, 3, 0, 3, 1, 3, 0, 1, 2, 3, 0, 1)
   True          72          0  (1, 0, 2, 0, 2, 0, 2, 3, 0, 2, 1, 3, 2, 3, 0, 1, 3, 0, 3, 1)
   True          10          0  (1, 3, 2, 3, 1, 3, 2, 0, 1, 2, 3, 0, 2, 0, 1, 1, 0, 2, 0, 3)
   True          48          0  (3, 1, 0, 1, 3, 0, 1, 2, 0, 3, 1, 2, 3, 2, 0, 1, 2, 0, 2, 3)
   True          44          0  (2, 3, 0, 3, 0, 3, 0, 1, 2, 3, 2, 1, 0, 1, 3, 2, 1, 3, 1, 2)
   True         164          0  (2, 3, 1, 3, 1, 3, 1, 0, 3, 1, 3, 0, 2, 0, 1, 3, 0, 2, 0, 3)
   True          13          0  (1, 3, 0, 3, 1, 3, 0, 2, 1, 3, 3, 2, 0, 2, 3, 1, 2, 0, 2, 1)
   True         441          0  (3, 0, 1, 0, 1, 0, 1, 2, 0, 3, 3, 3, 1, 3, 0, 1, 2, 0, 2, 1)
   True          24          0  (2, 0, 3, 0, 2, 1, 0, 1, 2, 3, 0, 3, 0, 3, 2, 0, 1, 3, 1, 0)
   True          11          0  (1, 3, 0, 3, 0, 0, 3, 2, 0, 1, 3, 1, 3, 1, 0, 0, 2, 1, 2, 3)
   True          40          0  (3, 0, 2, 0, 2, 1, 2, 1, 3, 2, 0, 0, 2, 0, 3, 0, 1, 3, 0, 2)
   True          20          0  (2, 0, 1, 0, 1, 1, 0, 3, 2, 1, 0, 3, 0, 3, 1, 0, 3, 2, 3, 0)
   True         363          0  (0, 1, 2, 1, 2, 3, 1, 3, 0, 1, 0, 2, 1, 2, 0, 0, 2, 1, 0, 3)
   True          26          0  (3, 1, 2, 1, 2, 1, 2, 0, 1, 3, 3, 0, 3, 2, 1, 2, 0, 1, 0, 3)
   True         104          0  (1, 0, 3, 0, 3, 3, 0, 2, 1, 0, 1, 2, 0, 1, 2, 1, 3, 0, 2, 1)
   True         163          0  (1, 3, 2, 3, 2, 3, 2, 0, 3, 2, 3, 0, 2, 1, 3, 3, 0, 1, 0, 2)
   True          19          0  (2, 0, 1, 0, 2, 3, 1, 3, 0, 2, 1, 2, 1, 2, 0, 1, 3, 0, 3, 1)
   True         149          0  (1, 0, 2, 0, 1, 2, 0, 3, 2, 0, 0, 3, 1, 3, 2, 1, 3, 2, 0, 1)
   True          21          0  (2, 0, 3, 0, 2, 3, 0, 1, 3, 2, 0, 1, 2, 1, 3, 0, 1, 3, 1, 0)
   True         236          0  (3, 0, 2, 0, 3, 2, 0, 1, 3, 0, 2, 1, 0, 3, 2, 3, 1, 0, 1, 3)
   True          31          0  (1, 0, 2, 0, 1, 0, 2, 3, 0, 2, 0, 3, 1, 3, 0, 0, 3, 2, 3, 1)
   True          45          0  (0, 3, 1, 3, 1, 3, 1, 2, 3, 0, 3, 0, 2, 1, 3, 3, 1, 0, 3, 2)
   True         182          0  (0, 3, 2, 3, 0, 2, 3, 1, 2, 3, 3, 0, 3, 0, 2, 0, 1, 2, 3, 0)
   True          16          0  (3, 2, 1, 2, 1, 2, 1, 0, 2, 3, 2, 0, 3, 0, 2, 2, 0, 3, 2, 1)
   True          28          0  (2, 0, 1, 0, 1, 1, 0, 3, 2, 0, 2, 3, 0, 3, 2, 2, 3, 0, 2, 1)
   True          16          0  (0, 3, 1, 3, 1, 2, 3, 2, 1, 0, 3, 0, 3, 0, 1, 3, 2, 0, 2, 3)
   True         103          0  (3, 1, 2, 1, 2, 2, 1, 0, 2, 3, 1, 0, 3, 0, 2, 2, 1, 3, 1, 0)
   True          28          0  (3, 2, 0, 2, 0, 1, 0, 1, 2, 3, 2, 3, 0, 2, 3, 0, 1, 3, 1, 2)
   True           7          0  (3, 0, 2, 0, 3, 0, 2, 1, 3, 2, 0, 1, 2, 1, 0, 0, 1, 2, 0, 3)
   True         138          0  (3, 2, 0, 2, 0, 2, 0, 1, 2, 3, 2, 3, 1, 3, 2, 2, 0, 3, 2, 1)
   True          17          0  (1, 3, 0, 3, 0, 0, 3, 2, 1, 0, 1, 2, 3, 2, 1, 1, 2, 3, 1, 0)
   True          58          0  (0, 1, 2, 1, 2, 2, 1, 3, 2, 0, 1, 3, 0, 1, 3, 2, 1, 0, 3, 2)
   True          18          0  (2, 3, 0, 3, 0, 0, 3, 1, 0, 3, 3, 2, 3, 2, 0, 0, 1, 2, 1, 3)
   True          34          0  (1, 3, 0, 3, 0, 0, 3, 2, 0, 3, 3, 1, 3, 1, 0, 0, 2, 1, 2, 3)
   True          10          0  (3, 2, 1, 2, 1, 2, 1, 0, 3, 2, 2, 0, 1, 0, 3, 1, 0, 3, 0, 1)
   True         227          0  (3, 0, 2, 0, 3, 0, 2, 1, 0, 3, 2, 3, 2, 3, 0, 2, 1, 0, 1, 2)
   True          10          0  (0, 3, 2, 3, 2, 1, 3, 1, 0, 3, 0, 2, 3, 2, 0, 0, 2, 3, 0, 1)
   True          32          0  (2, 1, 0, 1, 0, 0, 1, 3, 0, 1, 2, 3, 1, 3, 2, 0, 3, 1, 2, 0)
   True          55          0  (0, 2, 1, 2, 0, 1, 2, 3, 1, 2, 2, 0, 2, 0, 1, 0, 3, 1, 2, 0)
   True          35          0  (1, 0, 2, 0, 1, 3, 0, 3, 2, 0, 2, 1, 0, 2, 1, 2, 3, 0, 3, 2)
   True          23          0  (2, 3, 1, 3, 2, 1, 3, 0, 2, 3, 3, 0, 3, 0, 1, 2, 0, 1, 0, 3)
   True           9          0  (3, 1, 0, 1, 0, 2, 0, 2, 3, 0, 3, 1, 0, 3, 1, 3, 2, 1, 2, 3)
   True          50          0  (1, 2, 3, 2, 1, 3, 2, 0, 3, 2, 3, 1, 0, 1, 3, 3, 1, 2, 3, 0)
   True          32          0  (3, 2, 1, 2, 3, 1, 2, 0, 1, 2, 2, 0, 3, 0, 1, 3, 0, 1, 0, 3)
   True          56          0  (2, 3, 1, 3, 1, 3, 1, 0, 3, 2, 2, 0, 1, 0, 3, 1, 0, 3, 0, 1)
   True          35          0  (0, 3, 2, 3, 0, 1, 3, 1, 2, 3, 2, 0, 3, 0, 2, 2, 0, 3, 2, 1)
   True         119          0  (2, 0, 3, 0, 3, 1, 3, 1, 0, 2, 0, 2, 3, 2, 0, 3, 1, 2, 1, 3)
   True          18          0  (0, 2, 1, 2, 0, 3, 2, 3, 0, 2, 1, 1, 2, 0, 1, 1, 3, 2, 3, 0)
   True          22          0  (1, 0, 2, 0, 1, 2, 0, 3, 2, 0, 2, 1, 0, 1, 3, 2, 1, 0, 3, 2)
   True          14          0  (3, 1, 2, 1, 2, 2, 1, 0, 2, 1, 3, 3, 1, 0, 3, 3, 0, 1, 0, 2)
   True          14          0  (1, 3, 0, 3, 0, 0, 3, 2, 0, 1, 3, 2, 1, 2, 0, 0, 3, 1, 3, 2)
   True          18          0  (3, 0, 1, 0, 3, 1, 0, 2, 3, 1, 0, 2, 0, 2, 1, 0, 2, 1, 2, 3)
   True          47          0  (0, 3, 1, 3, 1, 1, 3, 2, 0, 3, 0, 2, 3, 0, 2, 0, 1, 3, 2, 0)
   True          53          0  (0, 3, 2, 3, 0, 3, 2, 1, 3, 0, 3, 1, 0, 1, 2, 3, 1, 2, 3, 0)
   True          69          0  (2, 0, 3, 0, 2, 3, 0, 1, 3, 0, 0, 1, 2, 1, 3, 2, 1, 3, 1, 2)
   True         133          0  (1, 0, 2, 0, 2, 0, 2, 3, 0, 1, 0, 3, 1, 2, 0, 0, 3, 1, 3, 2)
   True          19          0  (1, 3, 2, 3, 2, 2, 3, 0, 2, 1, 1, 0, 1, 0, 3, 2, 0, 3, 0, 1)
   True          10          0  (0, 1, 3, 1, 0, 3, 1, 2, 3, 1, 3, 2, 0, 2, 1, 3, 2, 1, 3, 0)
   True          10          0  (2, 1, 0, 1, 2, 3, 1, 3, 0, 1, 0, 2, 1, 2, 0, 0, 2, 1, 0, 3)
   True         128          0  (2, 1, 0, 1, 0, 1, 0, 3, 1, 2, 1, 2, 3, 0, 2, 1, 0, 2, 3, 1)
   True          23          0  (0, 2, 1, 2, 1, 3, 1, 3, 0, 1, 2, 2, 1, 2, 0, 2, 3, 0, 2, 1)
   True         123          0  (1, 0, 3, 0, 1, 0, 3, 2, 0, 3, 3, 1, 3, 1, 0, 1, 2, 0, 3, 1)
   True          86          0  (3, 2, 1, 2, 1, 1, 2, 0, 1, 2, 3, 3, 0, 2, 1, 1, 3, 2, 3, 0)
   True          13          0  (2, 3, 1, 3, 2, 1, 3, 0, 1, 3, 1, 0, 3, 2, 1, 1, 0, 3, 0, 2)
   True         107          0  (1, 0, 2, 0, 1, 3, 2, 3, 1, 2, 2, 0, 2, 0, 1, 1, 3, 0, 3, 2)
   True         402          0  (3, 0, 2, 0, 3, 0, 2, 1, 3, 2, 0, 1, 2, 3, 1, 3, 0, 2, 0, 3)
   True          27          0  (0, 1, 2, 1, 2, 1, 2, 3, 1, 0, 1, 0, 3, 0, 1, 1, 2, 0, 1, 3)
   True          33          0  (2, 0, 3, 0, 3, 0, 3, 1, 2, 0, 0, 1, 3, 1, 2, 3, 1, 2, 1, 3)
   True           8          0  (1, 3, 0, 3, 0, 0, 3, 2, 0, 1, 3, 2, 1, 2, 0, 0, 3, 1, 3, 2)
   True          11          0  (3, 2, 1, 2, 3, 0, 2, 0, 3, 2, 1, 1, 2, 1, 3, 3, 1, 2, 1, 0)
   True          21          0  (3, 1, 2, 1, 2, 2, 1, 0, 2, 1, 3, 0, 3, 0, 1, 2, 0, 1, 0, 3)
   True          23          0  (1, 3, 2, 3, 2, 2, 3, 0, 2, 1, 3, 1, 3, 1, 2, 2, 0, 1, 0, 3)
   True          12          0  (3, 1, 0, 1, 0, 2, 0, 2, 1, 3, 1, 3, 0, 1, 3, 0, 2, 3, 2, 0)
   True          12          0  (1, 3, 2, 3, 2, 2, 3, 0, 1, 2, 1, 0, 3, 0, 1, 1, 0, 3, 1, 2)
   True          22          0  (1, 3, 0, 3, 0, 0, 3, 2, 0, 1, 1, 2, 3, 2, 1, 0, 2, 3, 2, 0)
   True          10          0  (3, 2, 1, 2, 3, 2, 1, 0, 2, 1, 2, 0, 3, 1, 2, 3, 0, 1, 2, 3)
   True          82          0  (2, 3, 0, 3, 2, 0, 3, 1, 2, 3, 0, 1, 3, 2, 1, 2, 0, 3, 1, 2)
   True          96          0  (2, 1, 3, 1, 3, 1, 3, 0, 1, 3, 1, 0, 2, 0, 1, 1, 0, 2, 1, 3)
   True          18          0  (3, 0, 1, 0, 1, 1, 0, 2, 3, 0, 3, 2, 0, 2, 3, 3, 1, 0, 3, 2)
   True          50          0  (3, 2, 0, 2, 3, 1, 2, 1, 3, 0, 2, 0, 2, 0, 3, 3, 1, 0, 1, 2)
   True         233          0  (3, 2, 0, 2, 3, 0, 2, 1, 0, 2, 2, 3, 2, 3, 0, 3, 1, 0, 2, 3)
   True          41          0  (2, 3, 1, 3, 2, 1, 3, 0, 1, 2, 1, 0, 2, 3, 1, 1, 0, 3, 0, 2)
   True          10          0  (2, 1, 3, 1, 3, 3, 1, 0, 2, 1, 1, 0, 1, 0, 2, 3, 0, 2, 0, 3)
   True          21          0  (0, 2, 3, 2, 3, 1, 2, 1, 0, 3, 2, 3, 2, 3, 0, 2, 1, 0, 2, 3)
   True         302          0  (2, 3, 0, 3, 2, 3, 0, 1, 3, 2, 3, 2, 0, 2, 3, 3, 1, 0, 3, 2)
   True          10          0  (0, 1, 3, 1, 0, 2, 3, 2, 1, 0, 3, 0, 3, 0, 1, 3, 2, 1, 2, 3)
   True          73          0  (0, 2, 3, 2, 0, 2, 3, 1, 2, 0, 3, 1, 3, 1, 0, 3, 1, 2, 1, 3)
   True          11          0  (1, 2, 0, 2, 0, 0, 2, 3, 0, 2, 2, 3, 2, 3, 1, 0, 3, 1, 2, 0)
   True          14          0  (3, 2, 1, 2, 1, 1, 2, 0, 3, 2, 3, 0, 2, 3, 0, 3, 1, 2, 0, 3)
