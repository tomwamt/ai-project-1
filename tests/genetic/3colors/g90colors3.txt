Test Parameters:
	filename: graph90.txt
	numRuns: 1
	colors: 3
	max generations: 5000
	population size: 20
	mutation chance: 1.0
	tournament size: 10
	tournament chance: 0.5

Summary:
1 tests were run. 0.0% of them succeeded.
Of the successes, the average number iterations was 0.00 (min 99999999, max 0).
Of the failures, the average conflicts in the best result was 21.00 (min 21, max 21)

All results:
Success  Iterations  Conflicts  Color Sequence
  False        4999         21  (0, 1, 2, 1, 2, 0, 2, 2, 0, 0, 1, 0, 1, 2, 2, 1, 0, 2, 2, 0, 1, 1, 0, 2, 0, 0, 1, 1, 2, 0, 1, 0, 1, 2, 2, 1, 2, 2, 2, 2, 0, 2, 1, 1, 1, 1, 0, 0, 2, 2, 1, 0, 0, 2, 1, 1, 0, 0, 0, 1, 2, 1, 2, 2, 1, 0, 0, 2, 2, 2, 2, 1, 2, 2, 2, 1, 2, 1, 1, 0, 0, 2, 0, 1, 1, 2, 1, 0, 1, 2)
