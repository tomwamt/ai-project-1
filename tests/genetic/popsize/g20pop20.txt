Test Parameters:
	filename: graph20.txt
	numRuns: 300
	colors: 4
	max generations: 4000
	population size: 20
	mutation chance: 0.8
	tournament size: 10
	tournament chance: 0.5

Summary:
300 tests were run. 100.0% of them succeeded.
Of the successes, the average number iterations was 59.77 (min 6, max 558).
Of the failures, the average conflicts in the best result was 0.00 (min 999999999, max 0)

All results:
Success  Iterations  Conflicts  Color Sequence
   True          20          0  (3, 0, 2, 0, 3, 2, 0, 1, 3, 2, 0, 1, 0, 1, 2, 3, 1, 2, 0, 3)
   True          40          0  (1, 0, 2, 0, 2, 2, 0, 3, 2, 1, 0, 3, 1, 0, 2, 2, 3, 1, 3, 0)
   True          10          0  (3, 2, 0, 2, 0, 0, 2, 1, 0, 2, 3, 1, 2, 3, 1, 0, 3, 2, 3, 0)
   True          16          0  (0, 3, 2, 3, 0, 1, 2, 1, 3, 2, 3, 0, 2, 0, 3, 0, 1, 2, 1, 0)
   True          19          0  (0, 2, 1, 2, 1, 2, 1, 3, 2, 0, 2, 3, 1, 0, 2, 2, 3, 0, 2, 1)
   True          59          0  (0, 2, 1, 2, 1, 1, 2, 3, 1, 2, 0, 3, 2, 3, 1, 1, 0, 2, 0, 3)
   True          32          0  (3, 1, 0, 1, 0, 1, 0, 2, 1, 3, 1, 2, 3, 0, 1, 1, 2, 3, 1, 0)
   True          50          0  (1, 2, 3, 2, 1, 2, 3, 0, 1, 2, 3, 0, 3, 0, 1, 3, 0, 2, 0, 3)
   True          10          0  (0, 3, 2, 3, 0, 2, 3, 1, 2, 0, 3, 1, 0, 1, 3, 3, 1, 2, 1, 0)
   True          37          0  (0, 2, 3, 2, 0, 2, 3, 1, 2, 0, 3, 1, 3, 0, 2, 3, 1, 2, 1, 0)
   True          70          0  (0, 3, 1, 3, 1, 2, 3, 2, 0, 3, 3, 1, 3, 1, 0, 1, 2, 0, 2, 3)
   True         159          0  (1, 2, 0, 2, 1, 3, 2, 3, 1, 2, 2, 0, 2, 1, 0, 1, 3, 0, 3, 2)
   True          58          0  (2, 3, 1, 3, 1, 3, 1, 0, 2, 3, 3, 0, 1, 2, 3, 1, 0, 2, 3, 1)
   True          34          0  (2, 0, 3, 0, 2, 0, 3, 1, 0, 3, 0, 1, 3, 1, 0, 0, 2, 3, 0, 1)
   True          33          0  (3, 1, 0, 1, 0, 0, 1, 2, 0, 3, 1, 3, 1, 2, 3, 1, 2, 3, 1, 0)
   True          19          0  (1, 0, 3, 0, 1, 0, 3, 2, 0, 3, 0, 2, 3, 2, 1, 0, 2, 3, 2, 0)
   True         214          0  (0, 1, 3, 1, 0, 3, 1, 2, 3, 0, 1, 0, 1, 0, 3, 1, 2, 3, 2, 0)
   True          23          0  (0, 1, 2, 1, 2, 2, 1, 3, 2, 0, 1, 3, 0, 1, 2, 2, 3, 0, 3, 1)
   True          13          0  (1, 2, 3, 2, 3, 3, 2, 0, 3, 1, 2, 0, 2, 0, 1, 2, 0, 1, 2, 3)
   True          29          0  (3, 2, 0, 2, 3, 0, 2, 1, 0, 3, 0, 3, 2, 3, 0, 0, 1, 2, 1, 3)
   True          31          0  (0, 3, 2, 3, 2, 2, 3, 1, 2, 0, 3, 0, 1, 3, 0, 2, 3, 0, 1, 2)
   True          64          0  (2, 1, 3, 1, 2, 0, 1, 0, 2, 3, 3, 3, 1, 2, 3, 2, 0, 1, 3, 2)
   True         217          0  (3, 2, 1, 2, 3, 2, 1, 0, 2, 3, 1, 3, 1, 3, 2, 1, 0, 2, 0, 1)
   True          38          0  (3, 2, 1, 2, 1, 2, 1, 0, 2, 3, 2, 3, 0, 1, 2, 2, 1, 3, 2, 0)
   True          66          0  (1, 0, 2, 0, 1, 0, 2, 3, 0, 2, 0, 3, 1, 3, 0, 0, 3, 2, 3, 1)
   True          17          0  (1, 0, 2, 0, 1, 3, 0, 3, 1, 2, 2, 2, 0, 1, 2, 1, 3, 0, 3, 1)
   True          26          0  (3, 1, 2, 1, 3, 2, 1, 0, 3, 2, 2, 0, 1, 0, 2, 3, 0, 1, 0, 3)
   True          35          0  (0, 3, 1, 3, 1, 1, 3, 2, 0, 3, 0, 2, 3, 2, 0, 0, 1, 3, 0, 2)
   True          38          0  (0, 1, 2, 1, 0, 2, 1, 3, 2, 1, 1, 3, 1, 0, 2, 0, 3, 2, 3, 1)
   True          40          0  (1, 3, 2, 3, 2, 3, 2, 0, 3, 1, 3, 0, 1, 2, 3, 3, 0, 1, 0, 2)
   True          14          0  (2, 0, 3, 0, 2, 3, 0, 1, 3, 0, 0, 2, 0, 2, 3, 2, 1, 3, 1, 2)
   True          30          0  (2, 1, 0, 1, 2, 3, 1, 3, 0, 1, 0, 2, 1, 2, 0, 2, 3, 1, 0, 2)
   True          16          0  (3, 2, 0, 2, 0, 2, 0, 1, 2, 0, 3, 1, 3, 0, 2, 3, 1, 2, 1, 3)
   True          73          0  (2, 3, 0, 3, 0, 0, 3, 1, 0, 2, 3, 2, 3, 1, 2, 3, 1, 2, 3, 0)
   True         114          0  (2, 0, 1, 0, 1, 1, 0, 3, 1, 2, 0, 3, 2, 3, 1, 0, 3, 2, 3, 0)
   True          47          0  (3, 2, 0, 2, 0, 0, 2, 1, 0, 3, 3, 3, 2, 1, 3, 0, 1, 2, 1, 0)
   True          44          0  (0, 3, 1, 3, 0, 3, 1, 2, 3, 1, 3, 2, 0, 2, 1, 3, 2, 1, 3, 0)
   True          42          0  (1, 0, 3, 0, 1, 2, 3, 2, 1, 3, 0, 0, 3, 0, 1, 1, 2, 3, 2, 0)
   True          87          0  (0, 1, 2, 1, 0, 3, 1, 3, 0, 1, 2, 2, 1, 2, 3, 0, 2, 1, 3, 0)
   True          29          0  (0, 1, 3, 1, 3, 1, 3, 2, 1, 0, 1, 2, 0, 2, 1, 1, 3, 0, 1, 2)
   True          30          0  (3, 1, 2, 1, 2, 2, 1, 0, 2, 1, 1, 0, 3, 1, 2, 2, 0, 3, 0, 1)
   True          33          0  (3, 1, 0, 1, 3, 2, 1, 2, 0, 1, 0, 3, 1, 0, 3, 3, 2, 1, 2, 0)
   True          36          0  (1, 2, 3, 2, 3, 2, 3, 0, 2, 3, 2, 0, 1, 3, 2, 2, 0, 1, 2, 3)
   True          40          0  (1, 0, 2, 0, 1, 3, 0, 3, 2, 1, 0, 1, 0, 1, 2, 0, 3, 2, 3, 1)
   True         170          0  (3, 0, 1, 0, 3, 1, 0, 2, 1, 0, 0, 2, 0, 3, 1, 3, 2, 1, 0, 3)
   True         131          0  (0, 1, 3, 1, 3, 2, 3, 2, 1, 3, 0, 0, 3, 0, 1, 0, 2, 1, 2, 3)
   True         174          0  (3, 0, 1, 0, 3, 1, 0, 2, 1, 0, 0, 3, 0, 3, 1, 3, 2, 1, 0, 3)
   True          37          0  (1, 3, 0, 3, 0, 0, 3, 2, 0, 3, 3, 2, 1, 3, 0, 0, 2, 1, 2, 3)
   True         309          0  (0, 2, 1, 2, 0, 2, 1, 3, 0, 2, 1, 3, 1, 3, 2, 1, 3, 2, 1, 0)
   True          33          0  (1, 2, 3, 2, 3, 2, 3, 0, 2, 1, 2, 0, 1, 0, 3, 3, 0, 1, 0, 2)
   True          12          0  (0, 1, 2, 1, 2, 3, 2, 3, 1, 0, 1, 0, 2, 1, 0, 1, 3, 0, 3, 2)
   True          23          0  (2, 1, 3, 1, 2, 1, 3, 0, 2, 3, 1, 0, 3, 2, 0, 2, 1, 3, 1, 2)
   True          19          0  (0, 2, 3, 2, 0, 1, 3, 1, 2, 0, 3, 0, 3, 0, 2, 3, 1, 2, 1, 0)
   True          20          0  (3, 2, 1, 2, 3, 2, 1, 0, 2, 1, 2, 0, 1, 0, 2, 2, 3, 1, 2, 0)
   True          72          0  (0, 3, 1, 3, 1, 3, 1, 2, 3, 1, 3, 2, 1, 2, 0, 3, 2, 0, 2, 3)
   True          57          0  (0, 3, 2, 3, 2, 2, 3, 1, 2, 3, 0, 1, 0, 1, 3, 0, 1, 3, 0, 2)
   True          42          0  (2, 0, 3, 0, 3, 3, 0, 1, 3, 0, 0, 2, 0, 1, 2, 3, 1, 2, 1, 3)
   True          12          0  (3, 2, 0, 2, 3, 0, 2, 1, 0, 2, 0, 1, 3, 1, 2, 0, 1, 2, 1, 0)
   True         102          0  (0, 2, 1, 2, 1, 2, 1, 3, 2, 0, 0, 0, 1, 3, 2, 1, 3, 2, 3, 1)
   True          27          0  (3, 2, 1, 2, 1, 2, 1, 0, 2, 3, 2, 0, 3, 1, 2, 1, 0, 3, 0, 1)
   True          18          0  (2, 1, 0, 1, 2, 0, 1, 3, 0, 2, 1, 2, 1, 3, 2, 1, 3, 0, 3, 1)
   True          34          0  (3, 0, 2, 0, 2, 0, 2, 1, 0, 3, 0, 3, 1, 2, 3, 0, 2, 3, 1, 0)
   True          23          0  (2, 0, 1, 0, 2, 0, 1, 3, 0, 1, 0, 2, 1, 3, 0, 0, 3, 1, 0, 2)
   True          20          0  (1, 3, 2, 3, 1, 0, 3, 0, 1, 2, 3, 2, 3, 1, 2, 1, 0, 2, 3, 1)
   True          46          0  (2, 0, 3, 0, 2, 1, 0, 1, 3, 2, 0, 2, 0, 3, 2, 0, 1, 3, 1, 0)
   True          17          0  (3, 2, 1, 2, 1, 2, 1, 0, 3, 2, 3, 0, 1, 3, 2, 1, 0, 2, 0, 3)
   True          72          0  (3, 0, 2, 0, 2, 0, 2, 1, 0, 3, 3, 1, 3, 1, 0, 2, 1, 0, 1, 3)
   True          14          0  (2, 0, 1, 0, 1, 3, 1, 3, 0, 1, 0, 2, 1, 2, 0, 0, 3, 2, 0, 1)
   True          44          0  (0, 2, 3, 2, 3, 2, 3, 1, 2, 0, 2, 1, 0, 3, 1, 2, 3, 0, 1, 2)
   True          17          0  (3, 0, 2, 0, 3, 0, 2, 1, 0, 2, 0, 3, 1, 3, 2, 0, 3, 2, 1, 0)
   True          23          0  (3, 0, 1, 0, 3, 2, 0, 2, 1, 0, 1, 3, 0, 1, 3, 1, 2, 0, 2, 1)
   True          13          0  (2, 0, 3, 0, 2, 1, 0, 1, 3, 0, 0, 2, 0, 2, 3, 2, 1, 3, 1, 2)
   True          95          0  (0, 2, 1, 2, 0, 2, 1, 3, 0, 1, 2, 3, 1, 0, 3, 0, 2, 1, 2, 0)
   True          73          0  (0, 3, 2, 3, 0, 2, 3, 1, 0, 2, 3, 1, 3, 1, 2, 0, 1, 2, 3, 0)
   True          19          0  (3, 1, 0, 1, 0, 1, 0, 2, 1, 0, 3, 3, 0, 2, 3, 3, 2, 1, 3, 0)
   True          24          0  (1, 2, 0, 2, 1, 3, 2, 3, 1, 2, 2, 0, 2, 1, 0, 1, 3, 0, 3, 1)
   True          40          0  (1, 3, 0, 3, 0, 2, 0, 2, 3, 0, 1, 1, 0, 1, 3, 1, 2, 3, 2, 0)
   True          18          0  (1, 0, 3, 0, 3, 2, 3, 2, 1, 0, 0, 0, 3, 1, 0, 3, 2, 1, 2, 3)
   True          23          0  (2, 0, 1, 0, 1, 1, 0, 3, 1, 0, 0, 2, 0, 3, 2, 1, 3, 2, 3, 0)
   True          38          0  (3, 0, 2, 0, 2, 2, 0, 1, 2, 0, 3, 3, 1, 3, 0, 2, 3, 0, 3, 1)
   True         156          0  (1, 2, 0, 2, 0, 2, 0, 3, 2, 0, 2, 3, 1, 3, 2, 2, 3, 1, 3, 0)
   True          34          0  (2, 0, 3, 0, 3, 0, 3, 1, 2, 0, 2, 1, 3, 2, 0, 2, 1, 0, 2, 3)
   True          16          0  (3, 1, 0, 1, 3, 0, 1, 2, 3, 1, 0, 2, 1, 2, 3, 3, 0, 1, 0, 2)
   True          17          0  (1, 2, 3, 2, 3, 0, 2, 0, 3, 2, 1, 1, 2, 3, 1, 1, 0, 2, 0, 3)
   True          37          0  (0, 2, 3, 2, 0, 2, 3, 1, 0, 3, 2, 1, 3, 0, 2, 0, 1, 3, 2, 0)
   True          30          0  (1, 3, 0, 3, 0, 3, 0, 2, 1, 3, 3, 2, 0, 1, 3, 0, 2, 1, 3, 0)
   True          43          0  (1, 3, 0, 3, 0, 0, 3, 2, 0, 3, 1, 1, 3, 1, 0, 1, 2, 3, 2, 1)
   True          22          0  (0, 3, 2, 3, 0, 3, 2, 1, 3, 0, 3, 0, 2, 1, 3, 3, 1, 2, 3, 0)
   True          47          0  (1, 2, 0, 2, 0, 0, 2, 3, 0, 2, 1, 1, 3, 2, 0, 0, 1, 2, 1, 3)
   True          95          0  (3, 2, 1, 2, 1, 2, 1, 0, 2, 3, 2, 3, 0, 1, 3, 2, 1, 3, 2, 0)
   True          22          0  (1, 3, 0, 3, 1, 0, 3, 2, 0, 3, 3, 1, 3, 2, 0, 1, 2, 0, 3, 1)
   True          13          0  (0, 3, 2, 3, 2, 1, 3, 1, 0, 3, 0, 2, 3, 0, 2, 0, 1, 3, 1, 0)
   True          34          0  (2, 3, 1, 3, 2, 3, 1, 0, 3, 1, 1, 0, 2, 1, 3, 2, 0, 3, 1, 2)
   True         101          0  (3, 0, 1, 0, 3, 0, 1, 2, 3, 0, 1, 2, 1, 3, 0, 3, 2, 0, 2, 3)
   True          13          0  (2, 1, 3, 1, 3, 1, 3, 0, 1, 2, 1, 0, 2, 0, 1, 3, 0, 2, 1, 3)
   True          22          0  (3, 2, 0, 2, 0, 1, 2, 1, 3, 2, 2, 0, 2, 0, 3, 0, 1, 3, 2, 0)
   True          17          0  (0, 1, 3, 1, 3, 3, 1, 2, 3, 0, 1, 2, 0, 1, 2, 3, 1, 0, 1, 3)
   True          10          0  (2, 0, 1, 0, 1, 1, 0, 3, 1, 2, 2, 2, 0, 2, 1, 1, 3, 0, 3, 2)
   True          46          0  (3, 1, 0, 1, 0, 2, 1, 2, 3, 1, 3, 0, 1, 0, 3, 0, 2, 1, 3, 0)
   True          60          0  (0, 3, 2, 3, 0, 1, 2, 1, 0, 2, 3, 3, 2, 3, 1, 0, 3, 2, 3, 0)
   True         235          0  (3, 2, 0, 2, 0, 1, 0, 1, 3, 2, 2, 2, 0, 2, 3, 0, 1, 3, 2, 0)
   True          25          0  (1, 0, 2, 0, 2, 2, 0, 3, 2, 0, 1, 3, 1, 3, 0, 1, 3, 0, 1, 2)
   True          32          0  (2, 1, 0, 1, 2, 3, 1, 3, 0, 1, 1, 2, 1, 2, 0, 2, 3, 0, 3, 1)
   True          34          0  (0, 3, 1, 3, 0, 3, 1, 2, 3, 1, 3, 2, 0, 2, 3, 0, 2, 1, 2, 0)
   True          54          0  (0, 3, 2, 3, 2, 2, 3, 1, 2, 0, 3, 1, 3, 1, 0, 3, 1, 0, 1, 2)
   True          23          0  (3, 2, 0, 2, 0, 2, 0, 1, 3, 0, 2, 1, 0, 3, 2, 2, 1, 3, 2, 0)
   True          16          0  (3, 2, 1, 2, 1, 0, 2, 0, 3, 2, 2, 1, 2, 1, 3, 1, 0, 3, 0, 1)
   True          19          0  (0, 2, 3, 2, 3, 2, 3, 1, 2, 3, 0, 0, 3, 1, 2, 0, 1, 2, 0, 3)
   True           9          0  (0, 3, 1, 3, 0, 1, 3, 2, 1, 3, 1, 2, 3, 2, 1, 0, 2, 3, 1, 0)
   True          28          0  (2, 1, 3, 1, 3, 0, 3, 0, 1, 3, 1, 2, 3, 1, 2, 1, 0, 2, 1, 3)
   True          15          0  (0, 1, 3, 1, 0, 2, 1, 2, 0, 1, 1, 3, 1, 0, 3, 0, 2, 3, 2, 0)
   True         326          0  (2, 0, 3, 0, 2, 1, 0, 1, 2, 0, 3, 3, 0, 3, 1, 2, 3, 0, 1, 2)
   True          99          0  (3, 0, 1, 0, 1, 2, 0, 2, 3, 1, 0, 1, 0, 1, 3, 0, 2, 3, 2, 0)
   True          15          0  (2, 3, 0, 3, 2, 3, 0, 1, 3, 0, 0, 1, 2, 0, 3, 2, 1, 3, 0, 2)
   True         138          0  (2, 3, 0, 3, 0, 1, 0, 1, 2, 3, 3, 3, 0, 3, 2, 0, 1, 2, 3, 0)
   True          20          0  (3, 0, 2, 0, 3, 1, 2, 1, 3, 2, 0, 0, 2, 3, 0, 3, 1, 2, 1, 3)
   True          46          0  (1, 3, 0, 3, 0, 3, 0, 2, 3, 1, 3, 2, 0, 2, 1, 0, 2, 1, 2, 0)
   True         131          0  (1, 0, 2, 0, 2, 0, 2, 3, 0, 1, 1, 1, 2, 1, 0, 2, 3, 0, 1, 2)
   True         181          0  (1, 0, 2, 0, 1, 2, 0, 3, 2, 0, 2, 3, 1, 3, 2, 2, 3, 0, 3, 1)
   True         408          0  (3, 0, 2, 0, 3, 2, 0, 1, 2, 0, 2, 3, 1, 3, 0, 2, 3, 0, 2, 1)
   True          49          0  (1, 2, 3, 2, 3, 2, 3, 0, 2, 3, 1, 0, 1, 3, 2, 1, 0, 2, 0, 1)
   True          13          0  (0, 2, 1, 2, 1, 2, 1, 3, 0, 2, 0, 3, 1, 0, 2, 0, 3, 2, 3, 0)
   True         159          0  (0, 2, 3, 2, 3, 3, 2, 1, 3, 0, 2, 1, 0, 1, 3, 3, 1, 0, 1, 2)
   True         327          0  (0, 1, 2, 1, 2, 2, 1, 3, 2, 1, 0, 0, 3, 1, 2, 2, 0, 1, 0, 3)
   True          44          0  (2, 1, 3, 1, 2, 1, 3, 0, 1, 3, 3, 0, 2, 0, 1, 2, 0, 1, 0, 3)
   True          39          0  (1, 0, 2, 0, 2, 0, 2, 3, 0, 1, 1, 1, 2, 1, 0, 2, 3, 0, 1, 2)
   True         112          0  (0, 3, 1, 3, 0, 2, 3, 2, 0, 3, 3, 1, 3, 1, 0, 0, 2, 1, 2, 3)
   True          72          0  (2, 3, 0, 3, 0, 1, 3, 1, 2, 0, 3, 0, 3, 2, 0, 3, 1, 2, 1, 3)
   True          26          0  (0, 3, 2, 3, 2, 2, 3, 1, 2, 0, 3, 0, 1, 0, 2, 2, 3, 0, 3, 1)
   True          25          0  (1, 2, 0, 2, 0, 0, 2, 3, 0, 2, 1, 3, 1, 3, 2, 0, 3, 2, 1, 0)
   True          10          0  (2, 3, 0, 3, 2, 3, 0, 1, 3, 0, 0, 1, 2, 1, 3, 2, 1, 3, 1, 0)
   True         455          0  (2, 1, 0, 1, 2, 1, 0, 3, 1, 0, 0, 3, 2, 3, 1, 2, 3, 1, 3, 2)
   True          34          0  (1, 3, 0, 3, 1, 3, 0, 2, 3, 0, 3, 1, 2, 1, 3, 3, 1, 0, 3, 2)
   True          36          0  (2, 3, 0, 3, 2, 0, 3, 1, 2, 3, 0, 1, 3, 2, 1, 2, 0, 3, 1, 2)
   True          36          0  (0, 3, 2, 3, 2, 2, 3, 1, 0, 3, 3, 1, 3, 0, 2, 2, 1, 0, 1, 3)
   True         143          0  (1, 0, 2, 0, 2, 0, 2, 3, 1, 2, 1, 3, 2, 3, 0, 1, 3, 0, 3, 2)
   True          34          0  (3, 1, 0, 1, 3, 1, 0, 2, 3, 1, 0, 2, 0, 2, 1, 3, 2, 1, 0, 3)
   True         222          0  (2, 3, 1, 3, 2, 1, 3, 0, 1, 2, 3, 0, 3, 2, 1, 3, 0, 1, 3, 2)
   True          15          0  (3, 1, 2, 1, 2, 1, 2, 0, 3, 1, 3, 0, 2, 0, 1, 2, 0, 1, 3, 2)
   True         299          0  (1, 3, 2, 3, 1, 2, 3, 0, 1, 2, 3, 0, 3, 1, 2, 1, 0, 2, 0, 1)
   True          75          0  (0, 3, 2, 3, 2, 1, 2, 1, 3, 2, 3, 0, 2, 0, 3, 3, 1, 0, 1, 2)
   True         263          0  (1, 2, 3, 2, 3, 2, 3, 0, 2, 1, 2, 1, 0, 3, 2, 2, 3, 1, 2, 0)
   True           6          0  (2, 1, 3, 1, 3, 3, 1, 0, 2, 1, 2, 0, 1, 2, 3, 3, 0, 1, 0, 2)
   True          47          0  (2, 3, 1, 3, 2, 3, 1, 0, 3, 1, 3, 0, 1, 0, 3, 2, 0, 1, 3, 2)
   True          16          0  (0, 1, 2, 1, 0, 2, 1, 3, 2, 0, 2, 3, 0, 3, 1, 2, 3, 1, 3, 2)
   True          33          0  (0, 1, 3, 1, 3, 2, 3, 2, 1, 0, 1, 0, 3, 1, 0, 1, 2, 0, 1, 3)
   True          61          0  (3, 2, 0, 2, 0, 0, 2, 1, 0, 3, 2, 3, 1, 2, 3, 0, 2, 3, 2, 0)
   True          13          0  (3, 2, 1, 2, 3, 2, 1, 0, 2, 1, 1, 0, 1, 0, 2, 3, 0, 2, 0, 1)
   True           7          0  (1, 3, 2, 3, 2, 0, 3, 0, 1, 2, 1, 2, 3, 2, 1, 1, 0, 3, 0, 2)
   True          18          0  (3, 1, 2, 1, 2, 0, 1, 0, 2, 1, 3, 3, 1, 2, 3, 3, 0, 1, 3, 2)
   True          61          0  (0, 3, 2, 3, 0, 2, 3, 1, 2, 3, 3, 1, 0, 3, 2, 0, 1, 2, 1, 3)
   True          28          0  (1, 3, 2, 3, 2, 2, 3, 0, 1, 2, 3, 0, 3, 0, 1, 3, 0, 1, 3, 2)
   True          13          0  (3, 2, 1, 2, 3, 0, 1, 0, 3, 1, 2, 2, 1, 3, 0, 3, 2, 1, 0, 3)
   True          34          0  (0, 1, 2, 1, 0, 1, 2, 3, 1, 2, 1, 3, 0, 3, 2, 0, 3, 2, 3, 0)
   True          27          0  (1, 2, 3, 2, 1, 3, 2, 0, 1, 3, 3, 0, 2, 0, 3, 1, 0, 2, 3, 1)
   True          51          0  (2, 0, 3, 0, 3, 0, 3, 1, 0, 2, 2, 2, 3, 1, 0, 3, 1, 0, 1, 2)
   True          22          0  (0, 1, 2, 1, 0, 3, 2, 3, 1, 0, 2, 0, 2, 0, 1, 2, 3, 1, 2, 0)
   True          12          0  (0, 2, 3, 2, 0, 2, 3, 1, 0, 3, 3, 1, 3, 1, 2, 0, 1, 2, 3, 0)
   True          13          0  (2, 1, 3, 1, 3, 3, 1, 0, 3, 1, 2, 0, 2, 0, 1, 2, 0, 1, 2, 3)
   True          11          0  (0, 3, 1, 3, 0, 1, 3, 2, 1, 3, 3, 2, 3, 2, 0, 0, 2, 1, 2, 3)
   True          33          0  (3, 1, 2, 1, 3, 0, 1, 0, 2, 3, 2, 3, 1, 3, 2, 2, 0, 1, 0, 3)
   True          25          0  (1, 2, 3, 2, 1, 3, 2, 0, 1, 2, 2, 0, 2, 0, 3, 1, 0, 3, 0, 2)
   True         120          0  (3, 2, 0, 2, 0, 2, 0, 1, 2, 0, 3, 3, 0, 3, 2, 3, 1, 2, 3, 0)
   True          18          0  (1, 3, 2, 3, 2, 0, 2, 0, 1, 2, 1, 3, 2, 1, 3, 1, 0, 3, 0, 1)
   True          13          0  (3, 2, 1, 2, 3, 2, 1, 0, 2, 1, 2, 0, 1, 3, 0, 2, 3, 1, 0, 2)
   True          13          0  (3, 2, 0, 2, 3, 2, 0, 1, 3, 0, 0, 1, 0, 3, 2, 3, 1, 2, 1, 0)
   True         125          0  (2, 1, 0, 1, 0, 1, 0, 3, 1, 2, 1, 3, 2, 0, 1, 0, 3, 2, 1, 0)
   True         246          0  (3, 0, 2, 0, 3, 0, 2, 1, 3, 2, 0, 1, 2, 1, 3, 3, 0, 2, 0, 1)
   True          31          0  (2, 1, 0, 1, 2, 0, 1, 3, 0, 1, 0, 2, 3, 2, 0, 0, 2, 1, 0, 3)
   True          22          0  (3, 0, 2, 0, 3, 2, 0, 1, 2, 0, 2, 3, 0, 3, 2, 2, 3, 0, 2, 1)
   True          13          0  (2, 3, 0, 3, 2, 3, 0, 1, 2, 0, 3, 1, 0, 1, 2, 2, 3, 0, 3, 1)
   True          53          0  (3, 1, 2, 1, 2, 1, 2, 0, 1, 3, 1, 3, 2, 3, 1, 1, 0, 3, 1, 2)
   True          16          0  (0, 2, 3, 2, 0, 2, 3, 1, 2, 3, 3, 1, 0, 1, 3, 0, 1, 2, 1, 0)
   True          79          0  (0, 1, 3, 1, 0, 1, 3, 2, 1, 3, 1, 2, 0, 2, 3, 1, 2, 3, 2, 1)
   True          12          0  (3, 0, 2, 0, 3, 2, 0, 1, 2, 0, 2, 1, 0, 1, 2, 2, 1, 0, 1, 3)
   True          56          0  (3, 0, 1, 0, 1, 2, 1, 2, 3, 0, 0, 0, 1, 0, 3, 1, 2, 3, 0, 1)
   True          22          0  (3, 1, 2, 1, 3, 1, 2, 0, 3, 2, 1, 0, 2, 3, 0, 3, 1, 2, 1, 3)
   True          52          0  (3, 2, 1, 2, 1, 1, 2, 0, 1, 2, 2, 3, 2, 0, 3, 1, 0, 3, 0, 2)
   True          24          0  (1, 0, 2, 0, 1, 2, 0, 3, 2, 0, 2, 1, 0, 3, 2, 2, 1, 0, 2, 3)
   True          12          0  (2, 3, 1, 3, 2, 3, 1, 0, 3, 2, 3, 0, 1, 2, 3, 3, 0, 1, 0, 2)
   True          29          0  (3, 2, 1, 2, 3, 2, 1, 0, 2, 3, 1, 0, 1, 0, 2, 1, 0, 2, 1, 3)
   True          40          0  (2, 3, 0, 3, 2, 3, 0, 1, 3, 2, 0, 1, 2, 0, 3, 0, 1, 3, 1, 2)
   True          43          0  (2, 1, 3, 1, 2, 0, 1, 0, 2, 3, 3, 3, 1, 2, 3, 2, 0, 1, 0, 2)
   True         220          0  (0, 1, 3, 1, 3, 1, 3, 2, 1, 0, 0, 0, 3, 0, 1, 3, 2, 1, 2, 0)
   True         123          0  (0, 2, 3, 2, 3, 3, 2, 1, 3, 0, 2, 1, 0, 1, 2, 2, 1, 0, 1, 3)
   True          12          0  (0, 2, 3, 2, 3, 2, 3, 1, 2, 3, 0, 0, 3, 1, 0, 0, 1, 2, 0, 3)
   True          12          0  (3, 0, 1, 0, 1, 1, 0, 2, 1, 3, 3, 2, 0, 2, 1, 1, 2, 0, 2, 3)
   True          26          0  (0, 1, 2, 1, 0, 1, 2, 3, 0, 1, 1, 3, 2, 3, 0, 0, 3, 2, 3, 1)
   True          54          0  (0, 1, 3, 1, 3, 3, 1, 2, 3, 1, 0, 2, 1, 0, 2, 3, 0, 1, 0, 3)
   True          79          0  (1, 3, 0, 3, 0, 2, 3, 2, 0, 1, 3, 1, 3, 0, 1, 3, 2, 1, 3, 0)
   True         115          0  (0, 2, 1, 2, 1, 2, 1, 3, 2, 0, 2, 3, 1, 0, 2, 1, 3, 0, 2, 1)
   True         132          0  (2, 0, 3, 0, 3, 1, 0, 1, 3, 0, 2, 2, 0, 2, 1, 3, 2, 0, 1, 3)
   True           7          0  (3, 0, 1, 0, 3, 0, 1, 2, 3, 1, 1, 2, 1, 2, 3, 3, 2, 0, 2, 1)
   True          25          0  (2, 3, 0, 3, 0, 3, 0, 1, 3, 2, 3, 2, 1, 0, 3, 3, 0, 2, 3, 1)
   True          13          0  (2, 1, 0, 1, 2, 3, 0, 3, 2, 1, 0, 1, 0, 2, 1, 0, 3, 1, 3, 2)
   True         362          0  (0, 1, 2, 1, 2, 1, 2, 3, 1, 2, 0, 3, 2, 3, 1, 0, 3, 1, 0, 2)
   True         558          0  (2, 0, 1, 0, 2, 1, 0, 3, 2, 1, 0, 3, 0, 2, 1, 2, 3, 1, 0, 2)
   True          20          0  (3, 2, 1, 2, 1, 1, 2, 0, 1, 2, 3, 0, 2, 3, 0, 1, 3, 2, 0, 1)
   True          24          0  (2, 0, 1, 0, 1, 0, 1, 3, 0, 2, 0, 3, 2, 3, 0, 1, 3, 2, 3, 1)
   True          51          0  (3, 0, 2, 0, 3, 0, 2, 1, 0, 3, 0, 1, 3, 2, 0, 0, 1, 2, 0, 3)
   True          26          0  (1, 3, 2, 3, 1, 0, 3, 0, 2, 3, 2, 1, 3, 2, 0, 2, 1, 3, 0, 2)
   True          41          0  (1, 2, 3, 2, 3, 2, 3, 0, 2, 3, 1, 0, 3, 0, 2, 1, 0, 2, 0, 3)
   True          25          0  (0, 2, 3, 2, 3, 2, 3, 1, 2, 0, 2, 1, 0, 3, 2, 3, 1, 0, 2, 3)
   True          42          0  (0, 2, 1, 2, 1, 1, 2, 3, 1, 2, 0, 3, 2, 3, 1, 0, 3, 2, 3, 0)
   True          25          0  (3, 2, 0, 2, 0, 1, 0, 1, 3, 0, 3, 2, 0, 3, 2, 3, 1, 2, 3, 0)
   True          37          0  (0, 3, 2, 3, 0, 2, 3, 1, 2, 0, 3, 1, 0, 1, 2, 3, 1, 2, 1, 3)
   True          31          0  (2, 0, 1, 0, 1, 1, 0, 3, 1, 2, 0, 2, 0, 3, 2, 0, 3, 2, 0, 1)
   True          31          0  (2, 3, 0, 3, 0, 0, 3, 1, 0, 3, 2, 1, 3, 2, 0, 0, 2, 3, 2, 1)
   True         184          0  (1, 0, 2, 0, 2, 3, 0, 3, 1, 0, 1, 2, 0, 2, 1, 1, 3, 0, 1, 2)
   True          43          0  (2, 0, 1, 0, 1, 0, 1, 3, 0, 2, 0, 2, 3, 1, 2, 0, 1, 2, 3, 0)
   True          64          0  (0, 3, 1, 3, 1, 3, 1, 2, 3, 1, 3, 0, 1, 0, 3, 3, 2, 0, 3, 1)
   True          16          0  (1, 2, 3, 2, 3, 2, 3, 0, 2, 1, 2, 0, 1, 0, 2, 3, 0, 1, 2, 3)
   True          47          0  (3, 2, 1, 2, 3, 2, 1, 0, 2, 1, 1, 3, 1, 0, 2, 3, 0, 2, 0, 3)
   True          72          0  (0, 2, 3, 2, 0, 2, 3, 1, 2, 0, 3, 1, 0, 1, 3, 3, 1, 2, 3, 0)
   True          22          0  (0, 2, 3, 2, 0, 1, 3, 1, 0, 3, 2, 2, 3, 2, 1, 0, 2, 3, 1, 0)
   True          29          0  (3, 0, 2, 0, 3, 1, 2, 1, 0, 2, 2, 3, 2, 3, 0, 3, 1, 0, 2, 3)
   True          68          0  (1, 0, 3, 0, 3, 3, 0, 2, 3, 1, 0, 2, 0, 2, 1, 0, 2, 1, 0, 3)
   True          25          0  (1, 3, 2, 3, 2, 0, 3, 0, 1, 2, 1, 2, 3, 2, 1, 1, 0, 3, 1, 2)
   True          13          0  (0, 2, 3, 2, 0, 1, 2, 1, 0, 2, 3, 3, 2, 3, 0, 0, 3, 2, 3, 1)
   True          14          0  (1, 0, 3, 0, 1, 3, 0, 2, 3, 1, 0, 2, 0, 2, 1, 0, 2, 3, 2, 0)
   True          15          0  (2, 1, 3, 1, 2, 3, 1, 0, 3, 1, 1, 2, 1, 2, 3, 2, 0, 3, 0, 1)
   True          20          0  (1, 2, 3, 2, 1, 3, 2, 0, 3, 2, 2, 0, 1, 0, 3, 1, 0, 3, 2, 1)
   True          19          0  (1, 3, 2, 3, 2, 2, 3, 0, 2, 3, 1, 1, 3, 1, 0, 2, 1, 3, 1, 2)
   True          16          0  (1, 0, 2, 0, 1, 2, 0, 3, 2, 1, 0, 3, 1, 0, 2, 0, 3, 2, 3, 1)
   True          49          0  (0, 1, 2, 1, 0, 2, 1, 3, 0, 1, 1, 3, 1, 3, 0, 0, 3, 2, 3, 1)
   True          32          0  (1, 3, 2, 3, 1, 2, 3, 0, 2, 3, 2, 1, 0, 3, 2, 2, 1, 3, 2, 0)
   True          50          0  (3, 1, 2, 1, 2, 2, 1, 0, 3, 1, 3, 0, 1, 3, 0, 3, 2, 1, 0, 3)
   True          14          0  (2, 0, 1, 0, 1, 1, 0, 3, 1, 0, 2, 2, 3, 0, 1, 1, 2, 0, 2, 3)
   True          62          0  (3, 2, 0, 2, 0, 2, 0, 1, 3, 0, 3, 1, 0, 3, 2, 3, 1, 2, 3, 0)
   True          20          0  (0, 3, 2, 3, 2, 1, 2, 1, 0, 2, 3, 3, 2, 3, 0, 3, 1, 0, 1, 2)
   True          25          0  (0, 2, 1, 2, 0, 2, 1, 3, 2, 0, 2, 3, 0, 3, 1, 2, 3, 1, 2, 0)
   True         145          0  (2, 0, 1, 0, 2, 3, 0, 3, 1, 2, 0, 2, 0, 2, 1, 0, 3, 1, 3, 0)
   True          79          0  (2, 0, 3, 0, 2, 3, 0, 1, 2, 3, 3, 1, 0, 1, 3, 2, 1, 0, 3, 2)
   True         158          0  (1, 2, 0, 2, 0, 2, 0, 3, 2, 1, 2, 1, 3, 0, 2, 2, 0, 1, 2, 3)
   True          65          0  (1, 3, 0, 3, 0, 0, 3, 2, 0, 1, 3, 1, 2, 3, 1, 0, 3, 1, 3, 0)
   True          21          0  (1, 2, 0, 2, 0, 2, 0, 3, 2, 1, 2, 1, 3, 0, 1, 2, 0, 1, 2, 3)
   True          56          0  (0, 2, 3, 2, 0, 1, 3, 1, 0, 2, 3, 2, 3, 0, 2, 0, 1, 2, 3, 0)
   True         234          0  (3, 1, 2, 1, 2, 2, 1, 0, 3, 1, 1, 0, 1, 3, 2, 2, 0, 3, 0, 1)
   True          25          0  (0, 2, 1, 2, 1, 2, 1, 3, 0, 1, 0, 3, 1, 0, 2, 0, 3, 2, 3, 0)
   True          42          0  (3, 0, 2, 0, 3, 0, 2, 1, 0, 2, 0, 3, 2, 1, 0, 0, 1, 2, 0, 3)
   True          22          0  (3, 0, 1, 0, 1, 2, 1, 2, 0, 1, 0, 3, 1, 3, 0, 0, 2, 3, 0, 1)
   True          51          0  (2, 0, 3, 0, 3, 3, 0, 1, 3, 2, 0, 2, 1, 0, 3, 3, 0, 2, 0, 1)
   True          44          0  (0, 3, 2, 3, 2, 3, 2, 1, 3, 0, 3, 0, 1, 2, 0, 3, 2, 0, 3, 1)
   True          81          0  (1, 0, 3, 0, 3, 3, 0, 2, 1, 3, 1, 2, 0, 2, 1, 1, 2, 0, 1, 3)
   True          46          0  (2, 3, 0, 3, 2, 3, 0, 1, 3, 0, 3, 2, 0, 2, 3, 3, 1, 0, 3, 2)
   True          37          0  (3, 2, 1, 2, 3, 2, 1, 0, 3, 1, 2, 0, 1, 3, 0, 3, 2, 1, 2, 3)
   True          16          0  (3, 1, 2, 1, 3, 2, 1, 0, 2, 1, 1, 3, 1, 3, 2, 3, 0, 2, 0, 3)
   True         132          0  (3, 0, 2, 0, 2, 0, 2, 1, 0, 2, 3, 1, 3, 2, 0, 3, 1, 0, 1, 3)
   True          63          0  (3, 1, 0, 1, 0, 1, 0, 2, 1, 3, 1, 3, 0, 3, 1, 0, 2, 3, 2, 0)
   True          31          0  (2, 1, 3, 1, 3, 1, 3, 0, 1, 2, 1, 2, 3, 0, 2, 3, 0, 2, 1, 3)
   True          17          0  (2, 3, 0, 3, 2, 3, 0, 1, 3, 0, 3, 2, 1, 2, 0, 3, 2, 0, 1, 3)
   True          32          0  (0, 3, 1, 3, 1, 3, 1, 2, 3, 1, 0, 2, 0, 2, 3, 0, 2, 3, 0, 1)
   True          14          0  (0, 2, 1, 2, 1, 3, 2, 3, 1, 2, 0, 0, 2, 0, 1, 0, 3, 2, 3, 0)
   True          32          0  (3, 0, 1, 0, 1, 2, 1, 2, 0, 3, 0, 3, 1, 0, 3, 0, 2, 3, 2, 0)
   True         198          0  (1, 3, 2, 3, 1, 3, 2, 0, 3, 1, 2, 1, 2, 1, 3, 2, 0, 3, 2, 1)
   True          10          0  (0, 1, 2, 1, 2, 1, 2, 3, 0, 2, 0, 3, 2, 0, 1, 0, 3, 1, 3, 2)
   True          16          0  (2, 3, 0, 3, 0, 1, 0, 1, 3, 2, 2, 2, 0, 3, 2, 0, 1, 3, 2, 0)
   True          38          0  (1, 2, 3, 2, 3, 3, 2, 0, 3, 2, 2, 0, 1, 2, 3, 3, 0, 1, 0, 2)
   True         138          0  (1, 3, 0, 3, 0, 3, 0, 2, 3, 1, 1, 2, 0, 1, 3, 0, 2, 3, 1, 0)
   True         361          0  (3, 2, 1, 2, 3, 0, 1, 0, 3, 1, 2, 2, 1, 2, 0, 3, 2, 1, 0, 3)
   True          32          0  (0, 2, 3, 2, 0, 2, 3, 1, 2, 3, 2, 1, 0, 3, 2, 2, 1, 3, 2, 0)
   True          99          0  (3, 0, 2, 0, 3, 1, 0, 1, 3, 0, 2, 2, 0, 3, 2, 3, 1, 0, 1, 3)
   True          56          0  (1, 3, 0, 3, 1, 0, 3, 2, 0, 1, 3, 2, 1, 3, 0, 3, 2, 0, 2, 3)
   True          11          0  (1, 0, 3, 0, 1, 0, 3, 2, 0, 3, 0, 2, 3, 2, 0, 0, 1, 3, 0, 2)
   True         152          0  (0, 2, 1, 2, 0, 1, 2, 3, 1, 2, 2, 3, 0, 3, 2, 0, 3, 1, 3, 0)
   True          52          0  (3, 0, 2, 0, 3, 1, 0, 1, 3, 0, 2, 2, 0, 2, 1, 3, 2, 0, 2, 3)
   True          71          0  (2, 0, 3, 0, 3, 1, 0, 1, 2, 0, 2, 3, 0, 3, 1, 2, 3, 0, 1, 2)
   True          14          0  (0, 1, 2, 1, 2, 1, 2, 3, 1, 0, 1, 3, 2, 3, 0, 1, 3, 0, 1, 2)
   True          27          0  (1, 0, 2, 0, 2, 2, 0, 3, 2, 1, 0, 1, 3, 0, 1, 2, 0, 1, 0, 2)
   True          16          0  (2, 1, 0, 1, 2, 0, 1, 3, 2, 0, 1, 3, 1, 3, 0, 1, 3, 0, 1, 2)
   True          36          0  (1, 3, 0, 3, 0, 2, 3, 2, 1, 3, 1, 0, 3, 0, 2, 1, 0, 3, 2, 1)
   True          12          0  (1, 2, 3, 2, 1, 2, 3, 0, 2, 3, 2, 1, 3, 0, 1, 1, 0, 3, 0, 2)
   True          20          0  (0, 1, 2, 1, 2, 1, 2, 3, 1, 0, 1, 3, 0, 3, 1, 1, 3, 0, 1, 2)
   True          22          0  (1, 3, 2, 3, 1, 3, 2, 0, 3, 2, 3, 1, 0, 1, 2, 3, 1, 2, 0, 3)
   True          15          0  (3, 1, 0, 1, 0, 0, 1, 2, 0, 3, 1, 3, 2, 1, 3, 0, 1, 3, 1, 0)
   True          13          0  (0, 3, 1, 3, 0, 2, 1, 2, 0, 1, 3, 3, 1, 3, 0, 0, 2, 1, 2, 3)
   True         110          0  (1, 0, 3, 0, 3, 3, 0, 2, 3, 1, 0, 2, 0, 2, 1, 0, 2, 1, 0, 3)
   True          12          0  (1, 2, 0, 2, 1, 2, 0, 3, 2, 1, 0, 3, 0, 3, 1, 0, 3, 2, 3, 0)
   True          10          0  (0, 1, 3, 1, 0, 1, 3, 2, 0, 3, 3, 2, 3, 0, 1, 0, 2, 1, 2, 0)
   True          10          0  (1, 3, 0, 3, 0, 2, 3, 2, 1, 0, 3, 0, 3, 1, 0, 3, 2, 1, 2, 3)
   True          50          0  (3, 1, 2, 1, 2, 1, 2, 0, 1, 3, 3, 0, 3, 0, 1, 2, 0, 1, 3, 2)
   True          20          0  (3, 0, 2, 0, 2, 0, 2, 1, 0, 3, 0, 1, 2, 1, 0, 2, 1, 3, 0, 2)
   True          20          0  (1, 3, 2, 3, 1, 2, 3, 0, 2, 1, 3, 0, 1, 0, 3, 3, 0, 2, 3, 1)
   True          33          0  (3, 2, 1, 2, 1, 0, 2, 0, 3, 1, 2, 1, 2, 1, 3, 2, 0, 3, 0, 2)
   True          12          0  (3, 1, 2, 1, 3, 1, 2, 0, 1, 2, 2, 0, 3, 2, 1, 3, 0, 1, 0, 3)
   True          12          0  (3, 1, 2, 1, 2, 1, 2, 0, 1, 3, 1, 0, 3, 2, 1, 2, 0, 3, 1, 2)
   True          25          0  (1, 3, 0, 3, 0, 0, 3, 2, 0, 1, 3, 1, 2, 1, 0, 0, 3, 1, 3, 2)
   True          33          0  (1, 0, 3, 0, 3, 0, 3, 2, 0, 1, 0, 1, 2, 1, 0, 0, 3, 1, 0, 2)
   True          79          0  (3, 1, 0, 1, 3, 0, 1, 2, 0, 1, 1, 2, 3, 1, 0, 3, 2, 0, 1, 3)
   True          18          0  (1, 2, 0, 2, 1, 3, 0, 3, 1, 0, 2, 2, 0, 2, 3, 1, 2, 0, 2, 1)
   True          77          0  (0, 1, 2, 1, 2, 2, 1, 3, 2, 1, 0, 3, 1, 3, 0, 2, 3, 1, 3, 2)
   True          18          0  (0, 3, 1, 3, 0, 3, 1, 2, 3, 1, 3, 0, 2, 0, 3, 3, 0, 1, 3, 2)
   True          55          0  (3, 1, 0, 1, 0, 2, 0, 2, 3, 0, 1, 1, 0, 1, 3, 1, 2, 3, 2, 1)
   True         131          0  (3, 2, 1, 2, 1, 1, 2, 0, 1, 3, 2, 3, 0, 2, 1, 1, 2, 3, 2, 0)
   True          31          0  (0, 3, 1, 3, 1, 1, 3, 2, 1, 0, 3, 0, 2, 3, 1, 1, 3, 0, 3, 2)
   True          22          0  (1, 3, 0, 3, 0, 2, 0, 2, 3, 1, 3, 1, 0, 1, 3, 3, 2, 1, 2, 0)
   True          82          0  (3, 1, 0, 1, 0, 0, 1, 2, 0, 3, 1, 2, 3, 1, 2, 0, 1, 3, 2, 0)
   True          32          0  (3, 1, 2, 1, 2, 0, 2, 0, 1, 2, 3, 3, 2, 3, 1, 3, 0, 1, 0, 2)
   True          42          0  (0, 1, 2, 1, 2, 2, 1, 3, 0, 2, 0, 3, 1, 3, 2, 0, 3, 1, 3, 0)
   True         324          0  (1, 3, 2, 3, 1, 2, 3, 0, 2, 1, 3, 0, 1, 3, 2, 3, 0, 2, 0, 1)
