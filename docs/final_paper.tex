\documentclass{article}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{subcaption}
\usepackage{graphicx}
\graphicspath{{img/}}
\usepackage{hyperref}
\usepackage{algorithm}
\usepackage{algpseudocode}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}

\title{Final Report \\ \large Project \#1}
\author{Group 20:\\Jason Sanders, Travis Stone, Tom Warner}
\date{\today}

\begin{document}
\maketitle

\abstract{
Our experiments have shown that the Genetic Algorithm is the best way to go about solving the Graph Coloring Problem. It simply has enough variables that can be controlled by the user to optimize success rate and runtime. Meanwhile, Min-Conflicts is very unreliable even if it can get lucky and find a good solution quickly every now and again. Backtracking and its variants were prohibitively slow.}

\section{Problem Statement}
\paragraph{}
Given an arbitrary graph of nodes numbering in multiples of 10 between 10 and 100 such that an edge between two nodes does not intersect with any other edge, our assignment was to implement five different algorithims that would look for ways to assign a color each node so that no two connected nodes share the same color. These five algorithms - which are all oriented around the idea of solving problems through search - are Min-Conflicts, Simple Backtracking, Backtracking with Forward Checking, Backtracking with Constraint Propogation, and a Genetic Algorithm that implemented a Penalty Function and Tournament selection.

\subsection*{Problem Generator}
\paragraph{}
The problem generator takes the desired number of vertices as input and produces a randomized, planar graph with a large number of edges. The process utilized by the problem generator to make these graphs is the same as the one described in the project requirements. It is also outlined in Algorithm \ref{generator}.
\paragraph{}
The problem generator was implemented in Python 3. This allows other Python programs to import it and generate their own Graph Coloring Problems on the fly. Additionally, the problem generator can export a graph as a text file to be read from any program. It can also make a PNG image of the graph.

\begin{algorithm}
\caption{$\textsc{ProblemGenerator}(n)$}
\label{generator}
\begin{algorithmic}
\Require A number of vertices $n$
\Ensure A planar graph with $n$ vertices
\State $V \gets$ a set of $n$ random points in the interval $([0, 1], [0, 1])$, sorted by x-coordinate
\State $E \gets$ an empty set of edges
\While{there exists a line between any two elements of $V$ that does not intersect any edge in $E$}
	\State $v_1 \gets$ an element of $V$
	\State $v_2 \gets$ the point in $V$ nearest to $v_1$ such that a line from $v_1$ to $v_2$ does not intersect any edge in $E$.
	\State add an edge from $v_1$ to $v_2$ to $E$
\EndWhile
\State \Return $(V, E)$
\end{algorithmic}
\end{algorithm}

\subsection*{Hypotheses}
\paragraph{}
From the beginning of the assignment, we hypothesized the following performance for each algorithm.

\begin{itemize}
\item
\textsc{Min-Conflicts}: As it is a randomized algorithm, \textsc{Min-Conflicts} does not guarantee a solution to the problem even when given infinite runtime. While it may sometimes find solutions quickly, we think it will run the second worst of the set on average; ranking only above the genetic algorithm.
\item
\textsc{Simple Backtracking}: Because all backtracking types are deterministic, we think \textsc{Simple Backtracking} will perform better than \textsc{Min-Conflicts} and the \textsc{Genetic Algorithm}, but worse than its Forward Checking and Constraint Propagation older brothers.
\item
\textsc{Backtracking With Forward Checking}: We believe that this method will perform second best next to \textsc{Backtracking with Constraint Propagation}. This is partially because it guarantees a solution with a better set of rules than \textsc{Simple Backtracking}, and also because Constraint Propagation will provide a better set of rules to follow more quickly than any other algorithm.
\item
\textsc{Backtracking with Constraint Propagation}: We believe that this algorithm will provide us with the fastest results given its ability to create a better set of rules more quickly than other Backtracking variations as well as guarantee a solution.
\item
\textsc{Genetic Algorithm}: We believe this method is the least properly suited way of solving the Graph Coloring Problem. Because of the tendency of premature convergence, no guarantee of a solution, and the comparatively complex steps it involves, this algorithm will not provide a fast solution.
\end{itemize}

\section{Algorithms}

\subsection{Min-Conflicts}
\paragraph{}
Jason Sanders was responsible for designing and implementing our Min-Conflict algorithm.
\paragraph{}
The Min-Conflicts approach to solving the Graph Coloring Problem begins by asking the user which graph they would like to solve. It grabs the graph associated with the file name inputted by the user, then loads it into the program to reference later. Along with the name of the graph, the number of colors and how many iterations of the search the user wants to run through are taken from the command line as arguments.
\paragraph{}
Once the program starts, it will grab a node from the graph at random and check all the nodes connected to it. After tallying each appearance of a certain color, the program will take the color with the least representation in the surrounding nodes and do one of three things.
\begin{itemize}
\item
If there are no conflicts, the color of the node will remain the same.
\item
If there are conflicts and one color has the least representation in the surrounding nodes, the color of the selected node will be changed to that color.
\item
If there are conflicts and colors are tied for the least representation, the program will choose one of these colors at random and change the selected node to that color.
\end{itemize}
\paragraph{}
After this, a search through the graph will determine whether or not the current state is a valid solution. If not, it will move onto the next recoloring and continue to do so until either a solution is found or it hits the user-specified iterative limit.
\paragraph{}
The pseudo-code for Min-Conflicts can be found in in Algorithm \ref{min-conflict}

\begin{algorithm}
\caption{$\textsc{Min-Conflicts}(G, k)$}
\label{min-conflict}
\begin{algorithmic}
\Require A graph $G = (V, E)$ and a number of colors $k$
\Ensure A consistent $k$-coloring of $G$ if one could be found
\State $colors \gets \textsc{CreateInitalColoring}(G, k)$
\State $iterations \gets 0$
\While{$iterations$ is less than $maxIter$ AND $G$ is not consistently colored}
	\State $node \gets \textsc{ChooseNode}(V)$
	\If{$node$ is connected to a node of the same color in $colors$}
    	\State $nodeColor \gets \textsc{LeastConflictingColor}(G, node, k)$
    \EndIf
    \State increment $iterations$ by 1
\EndWhile
\State \Return $colors$
\end{algorithmic}
\end{algorithm}

\paragraph{}
The design decisions made during the planning and implementation of this algorithm included:
\begin{itemize}
\item
How to implement \textsc{CreateInitialColoration}
\item
How to implement \textsc{ChooseNode}
\item
How to implement \textsc{LeastConflictingColor}
\end{itemize}

\paragraph{}
With \textsc{CreateInitalColoring}, the simplest approach was to go through each node and assign it a random color based on the roll of a die. In problem generation, we're provided with an ordered list of nodes. This makes the process of going through and assigning each node a color doable in linear time.

\paragraph{}
With \textsc{ChooseNode}, the implementation is straightforward. We simply have to pick a random number between 0 and the number of G's nodes minus one. While a failsafe to avoid picking the same node twice in a row was considered in the Design Document, this feature did not make it through to the final code.

\paragraph{}
With \textsc{LeastConflictingColor}, the idea was to grab and enumerate the colors of each node connected to the current node. Then, the color in the list of all colors with the least representation among those nodes would be selected and the color of the node being focused on would be changed to that color. If there were ever two colors tied for least representation, then the new color for the node will be decided between the colors randomly.

\paragraph{}
The Min-Conflicts solution was be implemented in Python 3 using a single class that contains the methods for the proper execution of the solution. The graph data is loaded in from a text file or automatically generated by our Problem Generator, and the solution is illustrated using the Pillow Python library.

\subsection{Backtracking}
\paragraph{}
Travis Stone was responsible for designing and implementing our backtracking algorithm and variations

\paragraph{}
The three different backtracking algorithms were implemented in Java using two classes: Node and Backtracking. The Node class represents the various vertices in the graph, and the Backtracking class contains the methods for the 3 Backtracking algorithms. These methods are recursive by necessity. The Backtracking class also contains a method that checks whether a node and any of its adjacent nodes share a color as well as a method that builds the data structure of the map.

\paragraph{}
Each node has variables corresponding to each vertex's x-coordinate, y-coordinate, color, and index (an arbitrary value that is assigned when the vertex is added to the map for counting purposes). In addition to these variables, each node has an array of possible colors that each algorithm cycles through. The purpose of this array is to provide a flag when it is time to backtrack. For Backtracking with Constraint propagation, this array of possible colors also serves the purpose of constraining nodes that have not been assigned a color yet.

\paragraph{}
The Node class contains a few methods that perform basic functions. These methods include checking if a specific node is adjacent to another, checking if a node still has available colors to choose from, and being able to reset a node so that it has no color and the array of possible colorations for that node allows for all colors. The only complicated method is the setColor method. The method is overloaded so that if a color is input as a parameter, then it will just set the node to that color. However, if it is called with no parameters, then it selects a random color from the possible colors and sets the node to that color.

\subsubsection*{Simple Backtracking}
\paragraph{}
The Simple Backtracking method takes a node as input while selecting a random node as a starting point. It returns a boolean value. The method utilizes a do-while loop. In the do portion, the method sets the node to a color. It will then check if this color conflicts with any adjacent node's color. If there it is a conflict, then the node's color will be changed. If all the possible colors have been exhausted, then the method will return false. The method then checks if all the nodes in the graph have been assigned colors, and will return true if they have. Then, the method finds the next random, uncolored node in the graph. Finally, in the `while' section of the do-while loop, the method will call the negation of the simpleBacktracking method, inputting the new random node as a parameter. At the end of the method outside the do-while loop, there is a return true statement so that when the last node is colored each backtracking method will terminate.

\begin{algorithm}[p]
\caption{$\textsc{GraphColoringBacktracking}(G, k, node)$}
\label{backtrack-simple}
\begin{algorithmic}
\Require A graph $G = (V, E)$, and a number of colors $k$, and a starting node $node$
\Ensure TRUE if a consistent coloring could be found, FALSE otherwise
\For{each $color$ in $k$}
	\State $node.color \gets color$
	\If{$node$ does not conflict with a neighbor}
		\If{there are no uncolored nodes in $V$}
			\State \Return TRUE
		\Else
			\State $newNode \gets$ an uncolored node in $V$
			\State \Return $\textsc{GraphColoringBacktracking}(G, k, newNode)$
		\EndIf
	\EndIf
\EndFor
\State \Return FALSE
\end{algorithmic}
\end{algorithm}

\subsubsection*{Backtracking with Forward Checking}
\paragraph{}
The Backtracking With Forward Checking algorithm works almost identically to the backtracking method save for one notable addition. Each node also contains a list of possible colors - initially a set of all $k$ colors. When a color that does not conflict with any adjacent nodes in the graph is selected, the method will remove the color from the possible colors of the adjacent node. If any of the adjacent nodes possible colors becomes zero, then the method will add the color back to the adjacent nodes possible colors and return false.

\begin{algorithm}[p]
\caption{$\textsc{GraphColoringBacktrackingWithForwardChecking}(G, k, node)$}
\label{backtrack-forward}
\begin{algorithmic}
\Require A graph $G = (V, E)$, and a number of colors $k$, and a starting node $node$
\Ensure TRUE if a consistent coloring could be found, FALSE otherwise
\For{each $color$ in $node.possibleColors$}
	\State $node.color \gets color$
	\If{$node$ does not conflict with a neighbor}
		\If{there are no uncolored nodes in $V$}
			\State \Return TRUE
		\Else
			\State remove $color$ from possible colors of the neighbors of $node$
			\If{a neighbor of node has no possible colors}
				\State undo color removal
				\State \Return FALSE
			\EndIf
			\State $newNode \gets$ an uncolored node in $V$
			\State \Return $\textsc{GraphColoringBacktrackingWithForwardChecking}(G, k, newNode)$
		\EndIf
	\EndIf
\EndFor
\State \Return FALSE
\end{algorithmic}
\end{algorithm}

\subsubsection*{Backtracking with Constraint Propagation}
\paragraph{}
Backtracking With Constraint Propagation's method is somewhat different from the previous two methods, but the basic framework remains intact. Instead of assigning a color to the Node at the start of the do-while loop, the method creates an integer array called constraintArray where the index refers to a color and the value refers to the number of constraints. Then, the method cycles through each possible color. The loop checks if all colors have been tried, and if true returns false. Then, if the current color has a conflict with an adjacent node then the color is changed and the conflicting color's value in the constraint array is set to -1 so that it is not considered later. Finally, the loop counts how many adjacent nodes are constrained by the color, sets that value to the corresponding index in the constraintArray, and ends the iteration of the loop. After the loop terminates, the Node's color is set to the color with the smallest value in the constraintArray. The do-while loop the finishes like the other two loops; checking if all the nodes have been assigned, selecting the next uncolored node, and calling the next recursive method.

\begin{algorithm}[p]
\caption{$\textsc{GraphColoringBacktrackingWithConstraintPropagation}(G, k, node)$}
\label{backtrack-constraint}
\begin{algorithmic}
\Require A graph $G = (V, E)$, and a number of colors $k$, and a starting node $node$
\Ensure TRUE if a consistent coloring could be found, FALSE otherwise
\State $constraintArray \gets$ an array indexed by color
\For{each $color$ in $k$}
	$constraintArray[color] \gets$ the number of adjacent nodes that would be constrained if $node.color = color$
\EndFor
\For{each $color$ in $k$ in ascending order of $constraintArray[color]$}
	\State $node.color \gets color$
	\If{$node$ does not conflict with a neighbor}
		\If{there are no uncolored nodes in $V$}
			\State \Return TRUE
		\Else
			\State $newNode \gets$ an uncolored node in $V$
			\State \Return $\textsc{GraphColoringBacktrackingWithConstraintPropagation}(G, k, newNode)$
		\EndIf
	\EndIf
\EndFor
\State \Return FALSE
\end{algorithmic}
\end{algorithm}

\subsection{Genetic Algorithm}
\paragraph{}
Tom Warner was responsible for designing, implementing, and running experiments on our genetic algorithm.
\paragraph{}
The genetic algorithm solution to the graph coloring problem involves maintaining a population of complete assignments and then using the standard genetic algorithm approach to repeatedly select the ``best'' assignments and combine them into ``better'' solutions. The pseudo-code of this algorithm is outlined in Algorithm \ref{genetic}.

\begin{algorithm}
\caption{$\textsc{GraphColoringGeneticAlgorithm}(G, k)$}
\label{genetic}
\begin{algorithmic}
\Require A graph $G = (V, E)$ and a number of colors $k$
\Ensure A consistent $k$-coloring of $G$ if one could be found
\State $population \gets \textsc{GenerateInitialPopulation}(G, k)$
\While{$population$ does not contain a consistent $k$-coloring of $G$}
	\State $newPopulation \gets$ an empty collection
	\For{$i = 1$ \textbf{to} $\textsc{Size}(population)$}
		\State $x \gets \textsc{TournamentSelect}(population)$
		\State $y \gets \textsc{TournamentSelect}(population)$
		\State $child \gets \textsc{Reproduce}(x, y)$
		\If{$mutateChance$}
			\State $\textsc{Mutate}(child)$
		\EndIf
		\State add $child$ to $newPopulation$
	\EndFor
	\State $population \gets newPopulation$
\EndWhile
\State \Return the element of $population$ that is a consistent $k$-coloring of $G$
\end{algorithmic}
\end{algorithm}
\paragraph{}
The design decisions we needed to make consisted of:
\begin{itemize}
\item
How to implement \textsc{GenerateInitialPopulation}

\item
How to implement \textsc{TournamentSelect}

\item
How to implement \textsc{Reproduce}, including whether to return one child or two.

\item
How to implement \textsc{Mutate}
\end{itemize}
\paragraph{}
When implementing \textsc{GenerateInitialPopulation}, we decided that simpler is better and faster, and used a number of randomized colorings to initialize the population. Each individual consists of a random color assignment to each vertex, regardless of neighboring vertices. The number of individuals in the population is set as a tuning parameter.
\paragraph{}
Implementing the parent selection was relatively simple, as the project requirements specified we must use a tournament selection method. When implementing \textsc{TournamentSelect}, both the tournament size and the chance of selecting the strongest individual are exposed as tuning parameters.
\paragraph{}
The fitness function used to determine the fitness of an individual for selection is simply the number of conflicts in that coloring. A perfect individual is one who this function returns 0 for. Because we are trying to minimize the number of conflicts, a more ``fit'' individual is one who this fitness function returns a \textit{lower} number for. \textsc{TournamentSelect} therefore selects based on lowest value, and in fact, the entire algorithm is trying to minimize this value.
\paragraph{}
When implementing \textsc{Reproduce}, we chose a randomized one-point crossover that produces only 1 child. A random split point is selected, and a child is created using assignments from one parent on one side of the split, and assignments from the other parent on the other side. It is important to note that because the problem generator outputs vertices sorted by x-coordinate and the sequence of color assignments follows the same order as the vertices, this crossover roughly corresponds to a crossover in the graph itself. That means the reproduction process should only create or resolve conflicts along the edges cut at the crossover point.
\paragraph{}
To mutate the children, \textsc{Mutate} selects a random assignment in the coloring and setting it to a random value. This simple approach should add enough variation and diversity to ``climb over'' any local extrema. The chance for a mutation to occur is exposed as a tuning parameter.
\paragraph{}
The genetic algorithm will be implemented in Python 3, using a single function that defines additional internal functions for fitness, tournament selection, reproduction, and mutation. Graphs to be used can either be generated immediately by the problem generator, or loaded from saved files exported by the problem generator.

\section{Experimental Design \& Results}
\paragraph{}
Each of our algorithms was run on graphs of 10 different sizes a number of times. For the Min-Conflicts algorithm, we executed multiple runs on the same graph and calculated the average time to find a solution as well as the rate of a solution being found at all. For the Backtracking algorithms, we will run the algorithm multiple different graphs due to their deterministic nature. With the Genetic Algorithm, we performed multiple runs on the same set of graphs to measure runtime and success rate, but in this case we altered the four tuning parameters with each set of runs to monitor how runtime is affected.

\subsection{Min-Conflicts}
\paragraph{}
The experiments done with the Min-Conflicts algorithm all involved running several runs on the same graph in order to amass information about the number of decisions made in each run as well as the number of decisions made on successful runs. In the context of this algorithm, a "decision" is defined to be whenever the color of a node in the graph is changed. As such, each run of the algorithm was limited to 50,000 iterations of the search before it moved onto the next run.
\paragraph{}
The relative runtime of the Min-Conflicts algorithm was measured by the average number of decisions made per run as well as the average number of decisions made per successful run. This is an important distinction as this algorithm has a chance to bottleneck itself and never finish. Successful runs complete much more quickly than unsuccessful runs.
\paragraph{}
For the graphs with 10, 20, 30, 40, and 50 nodes, 50 renditions of the algorithm ran for each number of colors. For the graphs with 60, 70, 80, 90, and 100 nodes, 30 renditions of the algorithm ran for each number of colors. In total, 800 trials of the Min-Conflicts algorithm were run across the 20 different conditions.

\begin{table}[ht]
\begin{center}
\begin{tabular}{|c|c c c|}
\hline
Graph Size & Average Swaps & Success Rate (percentage) & Average Swaps on Success\\ \hline
10 & 12,018 & 0 & N/A\\
20 & 12,295 & 0 & N/A\\
30 & 13,225 & 0 & N/A\\
40 & 22,437 & 0 & N/A\\
50 & 18,746 & 0 & N/A\\
60 & 20,320 & 0 & N/A\\
70 & 23,770 & 0 & N/A\\
80 & 22,097 & 0 & N/A\\
90 & 22,579 & 0 & N/A\\
100 & 21,644 & 0 & N/A\\ \hline
\end{tabular}
\caption{3-Color Min-Conflicts Search Results}
\label{MC3C}
\end{center}
\end{table}

\begin{table}[ht]
\begin{center}
\begin{tabular}{|c|c c c|}
\hline
Graph Size & Average Swaps & Success Rate (percentage) & Average Swaps on Success\\ \hline
10 & 605 & 88 & 7\\
20 & 640 & 76 & 38\\
30 & 1,555 & 16 & 49\\
40 & 1,468 & 22 & 108\\
50 & 1,257 & 40 & 203\\
60 & 570 & 73 & 218\\
70 & 2,359 & 3 & 452\\
80 & 1,950 & 20 & 303\\
90 & 1,557 & 23 & 287\\
100 & 1922 & 10 & 449\\ \hline
\end{tabular}
\caption{4-Color Min-Conflicts Search Results}
\label{MC4C}
\end{center}
\end{table}
\paragraph{}
Looking at the data presented in Table \ref{MC3C} and Table \ref{MC4C} leads us to a few final conclusions. Firstly, every graph used for testing the algorithm was unable to be solved using only three colors, but there were varying rates of success with four colors. Secondly, as the complexity of the graph increased, the number of decisions needed to solve it increased as well. The only places where this is contradicted is for the graphs with 80 and 90 nodes, though this could be attributed to the graph with 70 nodes being a particularly hard one to solve. This attribution is supported by the 3 percent success rate - the lowest of any graph in the 4-Color tests. By this same logic, we can conclude that the graph with 60 nodes had a particularly easy set of solutions to find for this algorithm.
\paragraph{}
Still, the data is all over the place. This leads to the third conclusion. The Min-Conflicts algorithm can find solutions very quickly, but it is not nearly as reliable as other methods. This is especially true for large graphs where it heavily relies on luck to make good decisions and have a good random coloring to go off of at the start of its attempt at finding a solution.
\subsection{Backtracking}
\paragraph{}
Each variant of the Backtracking Algorithm was run 50 times each on graph sizes ranging from 10 to 40 nodes, for both 3 and 4 colors. Because of the exponentially increasing time required to run the algorithm, it was unfeasible to run the graph on the larger graphs. For each set of runs, the success rate and average number of decisions were recorded. Table \ref{BackSimple} records the results of Simple Backtracking, Table \ref{BackForward} records the results of Backtracking with Forward Checking, and Table \ref{BackMAC} records the results of Backtracking with Constraint Propagation.

\begin{table}[ht]
\begin{center}
\begin{tabular}{|c|c c c|}
\hline
Graph Size & Colors & Success Rate & Avg. Decisions \\ \hline
10 & 3 &   0\% & 603 \\
   & 4 & 100\% & 38 \\ \hline

20 & 3 &   0\% & 12210 \\
   & 4 & 100\% & 28008 \\ \hline
   
30 & 3 &   0\% & 185432 \\
   & 4 & 100\% & 264397 \\ \hline
   
40 & 3 &   0\% & 1907536 \\
   & 4 & 100\% & 20013420 \\ \hline
\end{tabular}
\caption{Simple Backtracking results}
\label{BackSimple}
\end{center}
\end{table}

\begin{table}[ht]
\begin{center}
\begin{tabular}{|c|c c c|}
\hline
Graph Size & Colors & Success Rate & Avg. Decisions \\ \hline
10 & 3 &   0\% & 1223 \\
   & 4 & 100\% & 109 \\ \hline

20 & 3 &   0\% & 26139 \\
   & 4 & 86\% & 11939 \\ \hline
   
30 & 3 &   0\% & 386330 \\
   & 4 & 90\% & 266604 \\ \hline
   
40 & 3 &   0\% & 4326074 \\
   & 4 & 2\% & 12215539 \\ \hline
\end{tabular}
\caption{Backtracking with Forward Checking results}
\label{BackForward}
\end{center}
\end{table}

\begin{table}[ht]
\begin{center}
\begin{tabular}{|c|c c c|}
\hline
Graph Size & Colors & Success Rate & Avg. Decisions \\ \hline
10 & 3 &   0\% & 1229 \\
   & 4 & 100\% & 110 \\ \hline

20 & 3 &   0\% & 26123 \\
   & 4 & 94\% & 7410 \\ \hline
   
30 & 3 &   0\% & 397769 \\
   & 4 & 96\% & 162977 \\ \hline
   
40 & 3 &   0\% & 4143178 \\
   & 4 & 14\% & 15031113 \\ \hline
\end{tabular}
\caption{Simple Backtracking results}
\label{BackMAC}
\end{center}
\end{table}

\subsection{Genetic Algorithm}
\paragraph{}
The four tuning parameters were population size, tournament size, tournament selection chance, and mutation chance. The primary variable being measured was ``relative runtime,'' which is defined as the population size times the number of generations elapsed. This corresponds to the number of times two parents were selected and a child was produced.
\paragraph{}
The other variable being measured was the percentage of trials that were successful in coloring the graph before the maximum generation limit was reached. Because larger graphs need more time to solve, the generation limit was set at 200 times the size of the graph in all experiments. Trials that failed to solve the graph were counted and recorded, but were not used in calculation of average relative runtime.
\paragraph{}
Due to the fact that larger graphs need more time to solve, the number of trials per experiment varies based on graph size. The number of trials per experiment is shown below.

\begin{center}
\begin{tabular}{r|c|c|c|c|c|c|c}
Graph Size & 10 & 20 & 30 & 40 & 50 & 60 & 70 \\ \hline
Trials & 400 & 300 & 200 & 100 & 50 & 20 & 10
\end{tabular}
\end{center}

\paragraph{}
All experiments involved the genetic algorithm attempting to color the graph with 4 colors.
\paragraph{}
In the first set of experiments, for graph sizes 10 to 70, the population size was varied while keeping mutation chance and tournament chance fixed at 80\% and 50\% respectively. Tournament size was kept at a fixed \textit{percentage} of 50\% the total population size. Results of these experiments are shown in Table \ref{popSizeResult}.

\begin{table}[ht]
\begin{center}
\begin{tabular}{|c|c c c|}
\hline
Graph Size & Population Size & Avg. Rel. Runtime & Success Rate \\ \hline
  & 10 & 154.5 & 100\% \\
   & 20 & 118.6 & 100\% \\
10 & 30 & 118.8 & 100\% \\
   & 40 & 122.0 & 100\% \\
   & 50 & 128.0 & 100\% \\ \hline

   & 10 & 1296.2 & 100\% \\
   & 20 & 1195.4 & 100\% \\
20 & 30 & 1664.7 & 100\% \\
   & 40 & 1727.6 & 100\% \\
   & 50 & 1860.5 & 100\% \\ \hline

   & 10 & 4473.3 & 100\% \\
   & 20 & 3819.6 & 100\% \\
30 & 30 & 5193.3 & 99.5\% \\
   & 40 & 4548.0 & 99.5\% \\
   & 50 & 4929.5 & 97.5\% \\ \hline

   & 10 & 28045 & 93\% \\
   & 20 & 15976 & 99\% \\
40 & 30 & 21168 & 95\% \\
   & 40 & 22804 & 98\% \\
   & 50 & 34771 & 93\% \\ \hline

   & 10 & 42752 & 72\% \\
   & 20 & 34458 & 96\% \\
50 & 30 & 44263 & 84\% \\
   & 40 & 51787 & 76\% \\
   & 50 & 45017 & 86\% \\ \hline

   & 10 & 69303 & 35\% \\
   & 20 & 20323 & 100\% \\
60 & 30 & 28155 & 100\% \\
   & 40 & 39825 & 95\% \\
   & 50 & 86516 & 95\% \\ \hline

   & 10 & N/A & 0\% \\
   & 20 & 77388 & 50\% \\
70 & 30 & 58415 & 60\% \\
   & 40 & 70853 & 60\% \\
   & 50 & 85436 & 70\% \\ \hline
\end{tabular}
\caption{Effects of varying population size}
\label{popSizeResult}
\end{center}
\end{table}

\paragraph{}
In the second set of experiments, for graph sizes 10 to 70, the tournament size was varied while population size was fixed at 25, tournament chance was fixed at 50\%, and mutation chance was fixed at 80\%. Results of these experiments are shown in Table \ref{tournSizeResult}.

\begin{table}[ht]
\begin{center}
\begin{tabular}{|c|c c c|}
\hline
Graph Size & Tournament Size & Avg. Rel. Runtime & Success Rate \\ \hline
   &  5 & 202.5 & 100\% \\
   & 10 & 135.0 & 100\% \\
10 & 15 & 109.3 & 100\% \\
   & 20 & 106.0 & 100\% \\
   & 25 & 112.0 & 100\% \\ \hline

   &  5 & 1631.8 & 100\% \\
   & 10 & 1299.8 & 100\% \\
20 & 15 & 1538.3 & 100\% \\
   & 20 & 1400.5 & 100\% \\
   & 25 & 1318.8 & 100\% \\ \hline

   &  5 & 5075.5 & 100\% \\
   & 10 & 5285.5 & 99.5\% \\
30 & 15 & 3954.3 & 99.0\% \\
   & 20 & 5087.8 & 99.0\% \\
   & 25 & 3915.0 & 98.5\% \\ \hline

   &  5 & 20720 & 100\% \\
   & 10 & 20012 & 96\% \\
40 & 15 & 17331 & 98\% \\
   & 20 & 17092 & 98\% \\
   & 25 & 17182 & 98\% \\ \hline

   &  5 & 37290 & 100\% \\
   & 10 & 43703 & 88\% \\
50 & 15 & 30613 & 88\% \\
   & 20 & 37958 & 86\% \\
   & 25 & 23798 & 86\% \\ \hline

   &  5 & 40861 & 100\% \\
   & 10 & 19958 & 100\% \\
60 & 15 & 37220 & 100\% \\
   & 20 & 29416 & 100\% \\
   & 25 & 29478 & 95\% \\ \hline

   &  5 & 99690 & 100\% \\
   & 10 & 77763 & 80\% \\
70 & 15 & 142750 & 40\% \\
   & 20 & 63213 & 60\% \\
   & 25 & 82450 & 80\% \\ \hline
\end{tabular}
\caption{Effects of varying tournament size}
\label{tournSizeResult}
\end{center}
\end{table}

\paragraph{}
In the third set of experiments, for graph sizes 10 to 70, the tournament selection chance was varied while the population size was fixed at 20, the tournament size fixed at 10, and the mutation chance at 80\%. Results of these experiments are shown in Table \ref{tournChanceResult}.

\begin{table}[ht]
\begin{center}
\begin{tabular}{|c|c c c|}
\hline
Graph Size & Tournament Chance & Avg. Rel. Runtime & Success Rate \\ \hline
   &  20\% & 287.2 & 100\% \\
   &  40\% & 131.0 & 100\% \\
10 &  60\% & 109.0 & 100\% \\
   &  80\% & 108.6 & 100\% \\
   & 100\% & 97.6 & 100\% \\ \hline

   &  20\% & 3898.6 & 100\% \\
   &  40\% & 1389.6 & 100\% \\
20 &  60\% & 1304.0 & 100\% \\
   &  80\% & 1255.6 & 100\% \\
   & 100\% & 1353.8 & 100\% \\ \hline

   &  20\% & 18693 & 100\% \\
   &  40\% & 4354.4 & 100\% \\
30 &  60\% & 4479.2 & 100\% \\
   &  80\% & 4300.0 & 99.5\% \\
   & 100\% & 3981.2 & 98.5\% \\ \hline
   
   &  20\% & 77096 & 33\% \\
   &  40\% & 17203 & 98\% \\
40 &  60\% & 13778 & 100\% \\
   &  80\% & 16829 & 93\% \\
   & 100\% & 14533 & 98\% \\ \hline
   
   &  20\% & 120790 & 8\% \\
   &  40\% & 32618 & 96\% \\
50 &  60\% & 41005 & 86\% \\
   &  80\% & 20313 & 80\% \\
   & 100\% & 18521 & 82\% \\ \hline
   
   &  20\% & N/A & 0\% \\
   &  40\% & 30417 & 100\% \\
60 &  60\% & 32118 & 95\% \\
   &  80\% & 24053 & 85\% \\
   & 100\% & 32291 & 100\% \\ \hline
   
   &  20\% & N/A & 0\% \\
   &  40\% & 90273 & 60\% \\
70 &  60\% & 86457 & 60\% \\
   &  80\% & 98444 & 90\% \\
   & 100\% & 71506 & 30\% \\ \hline
\end{tabular}
\caption{Effects of varying tournament chance}
\label{tournChanceResult}
\end{center}
\end{table}

\paragraph{}
In the final set of experiments, for graph sizes 10 to 70, the mutation chance was varied while the population size was fixed at 20, the tournament size was fixed at 10, and the tournament selection chance was fixed at 50\%. The results of these experiments are shown in Table \ref{mutateResult}.

\begin{table}[ht]
\begin{center}
\begin{tabular}{|c|c c c|}
\hline
Graph Size & Mutation Chance & Avg. Rel. Runtime & Success Rate \\ \hline
   &  20\% & 310.4 & 100\% \\
   &  40\% & 180.8 & 100\% \\
10 &  60\% & 135.4 & 100\% \\
   &  80\% & 119.6 & 100\% \\
   & 100\% & 112.2 & 100\% \\ \hline
   
   &  20\% & 6904.0 & 97.0\% \\
   &  40\% & 3233.8 & 100\% \\
20 &  60\% & 2033.0 & 100\% \\
   &  80\% & 1229.4 & 100\% \\
   & 100\% & 915.0 & 100\% \\ \hline
   
   &  20\% & 24252 & 95.5\% \\
   &  40\% & 11983 & 98.5\% \\
30 &  60\% & 6266.6 & 100\% \\
   &  80\% & 4528.8 & 100\% \\
   & 100\% & 2708.2 & 100\% \\ \hline
   
   &  20\% & 53108 & 71\% \\
   &  40\% & 37666 & 91\% \\
40 &  60\% & 23983 & 94\% \\
   &  80\% & 12968 & 98\% \\
   & 100\% & 9231.8 & 100\% \\ \hline
   
   &  20\% & 61649 & 56\% \\
   &  40\% & 61354 & 76\% \\
50 &  60\% & 37446 & 90\% \\
   &  80\% & 27505 & 94\% \\
   & 100\% & 15916 & 100\% \\ \hline
   
   &  20\% & 54590 & 40\% \\
   &  40\% & 79212 & 80\% \\
60 &  60\% & 51985 & 100\% \\
   &  80\% & 22451 & 90\% \\
   & 100\% & 14902 & 100\% \\ \hline
   
   &  20\% & 102030 & 30\% \\
   &  40\% & 90713 & 60\% \\
70 &  60\% & 103720 & 50\% \\
   &  80\% & 129510 & 80\% \\
   & 100\% & 45987 & 90\% \\ \hline
\end{tabular}
\caption{Effects of varying mutation chance}
\label{mutateResult}
\end{center}
\end{table}

\paragraph{}
After the tuning experiments were run, the algorithm was run once each on graphs of size 80, 90, 100, and 200. The 80 vertex graph was solved with a relative runtime of 59620, 90 with a runtime of 78740, 100 with a runtime of 43940, and 200 with a runtime of 114000.

\paragraph{}
Additionally, the algorithm was run once each on every graph, attempting to color them with only 3 colors. The only graph it succeeded on was a hand-made graph specifically designed to be 3-colorable.

\section{Conclusions}
\subsection{The Min-Conflicts Bottleneck}
\paragraph{}
To elaborate further on the Min-Conflicts Bottlenecking issue first mentioned in the Experimental Design, looking at the data provided by Table \ref{MC4C} reveals a harsh difference between the successful runs' number of decisions and the failed runs' number of decisions. If a graph does get solved by the algorithm, it does so within 1000 color swaps at a consistent rate.
\paragraph{}
There is also the matter of average rate of success to deal with. No matter what the graph, \textsc{Min-Conflicts} was unable to solve it when bound by 3 colors. Meanwhile, the 4 color bounded trials had sporadic rates of success when it worked with more complex graphs. While the 100 node graph had a 10 percent rate of success, the 70 node graph had a 3 percent rate of success. This showcases the generally unreliable nature of \textsc{Min-Conflicts} even when a solution exists.

\subsection{Backtracking}
\paragraph{}
The most immediate conclusion to note from testing the Backtracking algorithms is that the time required to run grows incredibly fast as the graphs grow larger, much more so than Min-Conflicts and the Genetic Algorithm. This exponential time increase made it impossible to even test larger graphs within a reasonable amount of time. From a performance standpoint, this stands out as a major drawback to these algorithms.
\paragraph{}
In comparing the Backtracking variants to each-other, we find that for very small graphs, the added overhead from Forward Checking and Constraint Propogation slows the algorithms down. However for the larger graphs, forward checking and constraint propagation cut down the number of decisions made significantly.
\paragraph{}
No Backtracking algorithms were able to use 3 colors to color the graph. This reinforces the results from the other algorithms.

\subsection{Genetic Algorithm Tuning Parameters}
\paragraph{}
The results shown in Table \ref{popSizeResult} suggest that a population size of approximately 20 is optimal for all graph sizes. While a larger population generally did decrease the number of generations to solve the graph, the extra calculation required to produce more children each generation made the total runtime larger.

\paragraph{}
Table \ref{tournSizeResult} is more difficult to analyze. It appears that increasing the tournament size slightly decreases the runtime in successful trials, but also leads to more failed trails. We suspect that this may be due to the fact that larger tournaments have a lower chance to select weaker members, which can increase the speed, but also decrease diversity and lead to premature convergence.

\paragraph{}
The primary conclusion we can draw from Table \ref{tournChanceResult} is that too low of a tournament selection chance will significantly hinder the algorithm, but the runtime levels off at approximately 40\%. Increasing the selection chance past 40\% does not seem to significantly impact the runtime. An interesting observation is that while 100\% selection chance should always select the strongest member and quickly decrease diversity, the midrange tournament size and high mutation rate used in the experiments may have been enough to keep diversity high.

\paragraph{}
Finally, the most profound effects came from varying the mutation rate, as seen in Table \ref{mutateResult}. High mutation rates significantly hasten the algorithm, especially on smaller graphs. This is likely because it is constantly keeping diversity at the level where it can quickly find a solution.

\paragraph{}
Overall, the optimal tuning parameters for the genetic algorithm appear to be a population size of 20, a tournament size of approximately half the population, a tournament selection chance of 40\% or above, and a mutation rate of 100\%.

\section{References}
[1] S.  Russell, P.  Norvig and E.  Davis, \textit{Artificial Intelligence}. Upper Saddle River, NJ: Prentice Hall, 2010.\\[0pt]
[2]``Tournament selection'', \textit{Wikipedia}, 2016. [Online]. Available: \url{https://en.wikipedia.org/wiki/Tournament_selection}. [Accessed: 16- Sep- 2016].

\end{document}