\documentclass{article}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{subcaption}
\usepackage{graphicx}
\graphicspath{{img/}}
\usepackage{algorithm}
\usepackage{algpseudocode}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}

\title{Design Document \\ \large Project \#1}
\author{Group 20:\\Jason Sanders, Travis Stone, Tom Warner}
\date{\today}

\begin{document}
\maketitle

\section{Problem Statement}
\paragraph{}
Given an arbitrary graph with nodes numbering in multiples of 10 from 10 to 100, we have been tasked with applying five different problem solving strategies to determine whether the graph can be colored with three or four different colors such that no adjacent verteces of it share a color. The five methods are all oriented around the idea of solution through search. They are: Min-Conflicts, Simple Backtracking, Backtracking with Forward Checking, Backtracking with Constraint Propagation (MAC), and Local Search using a Genetic Algorithm with a Penalty Function and Tournament Selection.

\subsection*{Problem Generator}
\paragraph{}
The problem generator generates a randomized, planar graph with a large number of edges. It takes the number of vertices in the graph as input. The process used to create the graph is the same as described in the project requirements, outlined below in Algorithm \ref{generator}.
\paragraph{}
The problem generator is implemented in Python 3. This allows it to be included from other Python files to generate graphs and use them immediately. Additionally, it can export the graphs as a text file to be read from any program, as well as generate a PNG image of the graph.

\begin{algorithm}
\caption{$\textsc{ProblemGenerator}(n)$}
\label{generator}
\begin{algorithmic}
\Require A number of vertices $n$
\Ensure A planar graph with $n$ vertices
\State $V \gets$ a set of $n$ random points in the interval $([0, 1], [0, 1])$
\State $E \gets$ an empty set of edges
\While{there exists a line between any two elements of $V$ that does not intersect any edge in $E$}
	\State $v_1 \gets$ an element of $V$
	\State $v_2 \gets$ the point in $V$ nearest to $v_1$ such that a line from $v_1$ to $v_2$ does not intersect any edge in $E$.
	\State add an edge from $v_1$ to $v_2$ to $E$
\EndWhile
\State \Return $(V, E)$
\end{algorithmic}
\end{algorithm}

\subsection*{Hypotheses}
\paragraph{}
We predict the following for the performance of each algorithm:

\begin{itemize}
\item
Min-Conflicts: As it is a randomized algorithm, \textsc{Min-Conflicts} does not guarantee a solution to the problem even when given infinite runtime. While it may sometimes find solutions quickly, on average, we think it will run the second worst of the set - ranking only above the genetic algorithm.
\item
Simple Backtracking: Because all backtracking types are deterministic and guarantee a solution, we think Simple Backtracking will perform better than \textsc{Min-Conflicts} and Genetic Algorithm, but worse than its Forward Checking and Constraint Propagation older brothers.
\item
Backtracking With Forward Checking: We believe that this method will perform second best next to Backtracking with Constraint Propagation. This is partially because it guarantees a solution with a better set of rules than Simple Backtracking, and also because Constraint Propagation will provide a better set of rules to follow more quickly than any other algorithm.
\item
Backtracking with Constraint Propagation: We believe that this algorithm will provide us with the fastest results given its ability to create a better set of rules more quickly than other Backtracking variations as well as guarantee a solution.
\item
Genetic Algorithm: We believe this method is the least properly suited way of solving the Graph Coloring Problem. Because of the tendency of premature convergence, no guarantee of a solution, and the comparatively complex steps it involves, this algorithm will not provide a fast solution.
\end{itemize}

\section{Algorithms}

\subsection{Min-Conflicts}
\paragraph{}
Jason Sanders is responsible for designing and implementing our Min-Conflict algorithm.
\paragraph{}
The Min-Conflicts Solution to the Graph Coloring Problem starts out by asking the user which graph they would like to test. Once a graph is selected, the file for that graph's data is read into the code and referenced later on. For now, we need the user to select how many colors they want to try solving the graph with. The valid inputs for this domain are three and four - if three, then red, blue, and green will be used to fill in the graph. If four is selected, yellow will also be included.
\paragraph{}
Once the user has selected their specified graph and the number of colors, the code will then begin randomly assigning colors down the line without caring for conflicts in the rules of the system. It will do this by creating a new array with one index for each point, and these points will correspond to the listings on the adjacency matrix provided in the graph's file. Each color will be assigned a color based on the roll of a random int (0 - Blue, 1 - Green, 2 - Red, 3 - Yellow). From there, we can begin executing the min conflicts method proper.
\paragraph{}
The method is iterative and will repeat a number of times specified by the user or until a solution is found. The basic outline of its run expressed through the pseudo-code in Algorithm \ref{min-conflict}

\begin{algorithm}
\caption{$\textsc{GraphColoringMinConflict}(G, k)$}
\label{min-conflict}
\begin{algorithmic}
\Require A graph $G = (V, E)$ and a number of colors $k$
\Ensure A consistent $k$-coloring of $G$ if one could be found
\State $colors \gets \textsc{CreateInitalColoring}(G, k)$
\State $iterations \gets 0$
\While{$iterations$ is less than $maxIter$ AND $G$ is not consistently colored}
	\State $node \gets \textsc{ChooseNode}(V)$
	\If{$node$ is connected to a node of the same color in $colors$}
    	\State $nodeColor \gets \textsc{LeastConflictingColor}(G, node, k)$
    \EndIf
    \State increment $iterations$ by 1
\EndWhile
\State \Return $colors$
\end{algorithmic}
\end{algorithm}

\paragraph{}
The design decisions made during the planning of this algorithm include:
\begin{itemize}
\item
How to implement \textsc{CreateInitialColoration}
\item
How to implement \textsc{ChooseNode}
\item
How to implement \textsc{LeastConflictingColor}
\end{itemize}

\paragraph{}
With \textsc{CreateInitalColoring}, the simplest approach is to go through each node and assign it a random color based on the roll of a die. In problem generation, we're provided with an ordered list of nodes. This makes the problem of going through and assigning each a color doable in linear time.

\paragraph{}
With \textsc{ChooseNode}, the implementation is straightforward. We simply have to pick a random number between 0 and the number of G's nodes minus one. To avoid picking the same node twice in a row, a simple fix can be added in to make sure the current node number does not equal the next node number before the change is made.

\paragraph{}
With \textsc{LeastConflictingColor}, the idea will be to grab and enumerate the colors of each node connected to the current node. Then, the color in k with the least representation among those nodes will be selected and the color of the node being focused on will be changed to that color. If there are two colors tied for least representation, then the new color for the node will be decided between the two randomly.

\paragraph{}
The Min-Conflicts solution will be implemented in Python 3 using a single class that contains the methods for the proper execution of the solution. The graph data will be loaded in from text files or automatically generated by our Problem Generator, and the solution will be picturized using the Pillow Python library.

\subsection{Backtracking}
\paragraph{}
Travis Stone is responsible for designing and implementing our backtracking algorithm and variations

\paragraph{}
The three different backtracking algorithms will be implemented in Java using two classes: Node and Backtracking. The Node class will represent the various vertices in the graph, and the Backtracking class will contain the methods for the 3 backtracking algorithms. Each method will be recursive. The Backtracking class will also contain a method that checks whether a node and any of its adjacent nodes share a color as well as a method for building the data structure for the map.

\paragraph{}
Each node will have variables corresponding to each vertex's x-coordinate, y-coordinate, color, and index (an arbitrary value that is assigned when the vertice was added to the map for counting purposes). In addition to these, each node will have an array of possible colors that each algorithm will cycle through. The purpose of that array will be to provide a notice when it's time to backtrack. For the backtracking with constraint propagation, this array of possible colors will also serve the purpose of constraining nodes that have not been assigned a color yet.

\paragraph{}
The Node class will contain a few methods that perform basic functions. These methods include checking if a specific node is adjacent to another, checking if a node still has available colors to choose from, and being able to reset a node so that it has no color and all of its possible colors array allows for all colors. The only complicated method is the setColor method. The method is overloaded so that if color is input as a parameter then it will just set the node to that color, but if it is called with no parameters then it selects a random color from the possible colors and sets the node to that color.

\subsubsection*{Simple Backtracking}
\paragraph{}
The Simple Backtracking method will take a node as input(selecting a random node as a starter) and return a boolean value. The method utilizes a do-while loop. In the do portion, the method will set the node to a color. It will then check if this color conflicts with any node's colors it is adjacent to. If there it is a conflict then the node's color will be changed, and if all the possible colors have been exhausted then the method will return false. The method then will check if all the nodes in the graph have been assigned colors, and will return true if they have. Then the method will find the next random, uncolored node in the graph. Finally in the `while' section of the do-while loop, the method will call the negation of the simpleBacktracking method, inputting the new random node as a parameter. At the end of the method outside the do-while loop there will be a return true statement so that when the last node is colored each backtracking method will terminate.

\begin{algorithm}[p]
\caption{$\textsc{GraphColoringBacktracking}(G, k, node)$}
\label{backtrack-simple}
\begin{algorithmic}
\Require A graph $G = (V, E)$, and a number of colors $k$, and a starting node $node$
\Ensure TRUE if a consistent coloring could be found, FALSE otherwise
\For{each $color$ in $k$}
	\State $node.color \gets color$
	\If{$node$ does not conflict with a neighbor}
		\If{there are no uncolored nodes in $V$}
			\State \Return TRUE
		\Else
			\State $newNode \gets$ an uncolored node in $V$
			\State \Return $\textsc{GraphColoringBacktracking}(G, k, newNode)$
		\EndIf
	\EndIf
\EndFor
\State \Return FALSE
\end{algorithmic}
\end{algorithm}

\subsubsection*{Backtracking with Forward Checking}
\paragraph{}
The Backtracking With Forward Checking algorithm will work almost identically to the backtracking method except for one notable addition. Each node will also contain a list of possible colors, initially a set of all $k$ colors. When a color that does not conflict with any adjacent nodes in the graph is selected, the method will remove the color from the possible colors of the adjacent node. If any of the adjacent nodes possible colors becomes zero then the method will add the color back to the adjacent nodes possible colors and return false.

\begin{algorithm}[p]
\caption{$\textsc{GraphColoringBacktrackingWithForwardChecking}(G, k, node)$}
\label{backtrack-forward}
\begin{algorithmic}
\Require A graph $G = (V, E)$, and a number of colors $k$, and a starting node $node$
\Ensure TRUE if a consistent coloring could be found, FALSE otherwise
\For{each $color$ in $node.possibleColors$}
	\State $node.color \gets color$
	\If{$node$ does not conflict with a neighbor}
		\If{there are no uncolored nodes in $V$}
			\State \Return TRUE
		\Else
			\State remove $color$ from possible colors of the neighbors of $node$
			\If{a neighbor of node has no possible colors}
				\State undo color removal
				\State \Return FALSE
			\EndIf
			\State $newNode \gets$ an uncolored node in $V$
			\State \Return $\textsc{GraphColoringBacktrackingWithForwardChecking}(G, k, newNode)$
		\EndIf
	\EndIf
\EndFor
\State \Return FALSE
\end{algorithmic}
\end{algorithm}

\subsubsection*{Backtracking with Constraint Propagation}
\paragraph{}
Backtracking With Constraint Propagation's method will be somewhat different from the previous two methods, but the basic framework is intact. Instead of assigning a color to the Node at the start of the do-while loop the method creates an integer array called constraintArray, where the index refers to a color and the value refers to the number of constraints. Then the method cycles through each possible color. The loop checks if all colors have been tried, and if true returns false. Then, if the current color has a conflict with an adjacent node then the color is changed and the conflicting color's value in the constraint array is set to -1(so that it is not considered later). Finally the loop counts how many adjacent nodes are constrained by the color, and sets that value to the corresponding index in the constraintArray and ends the iteration of the loop. After the loop terminates the Node's color is set to the color with the smallest value in the constraintArray. The do while loop the finishes like the other two loops; checking if all the nodes have been assigned, selecting the next uncolored node, and calling the next recursive method.

\begin{algorithm}[p]
\caption{$\textsc{GraphColoringBacktrackingWithConstraintPropagation}(G, k, node)$}
\label{backtrack-constraint}
\begin{algorithmic}
\Require A graph $G = (V, E)$, and a number of colors $k$, and a starting node $node$
\Ensure TRUE if a consistent coloring could be found, FALSE otherwise
\State $constraintArray \gets$ an array indexed by color
\For{each $color$ in $k$}
	$constraintArray[color] \gets$ the number of adjacent nodes that would be constrained if $node.color = color$
\EndFor
\For{each $color$ in $k$ in ascending order of $constraintArray[color]$}
	\State $node.color \gets color$
	\If{$node$ does not conflict with a neighbor}
		\If{there are no uncolored nodes in $V$}
			\State \Return TRUE
		\Else
			\State $newNode \gets$ an uncolored node in $V$
			\State \Return $\textsc{GraphColoringBacktrackingWithConstraintPropagation}(G, k, newNode)$
		\EndIf
	\EndIf
\EndFor
\State \Return FALSE
\end{algorithmic}
\end{algorithm}

\subsection{Genetic Algorithm}
\paragraph{}
Tom Warner is responsible for designing, implementing, and running experiments on our genetic algorithm.
\paragraph{}
The genetic algorithm solution to the graph coloring problem involves maintaining a population of complete assignments and then using the standard genetic algorithm approach to repeatedly select the ``best'' assignments and combine them into ``better'' solutions. The pseudo-code of this algorithm is outlined in Algorithm \ref{genetic}.

\begin{algorithm}
\caption{$\textsc{GraphColoringGeneticAlgorithm}(G, k)$}
\label{genetic}
\begin{algorithmic}
\Require A graph $G = (V, E)$ and a number of colors $k$
\Ensure A consistent $k$-coloring of $G$ if one could be found
\State $population \gets \textsc{GenerateInitialPopulation}(G, k)$
\While{$population$ does not contain a consistent $k$-coloring of $G$}
	\State $newPopulation \gets$ an empty collection
	\For{$i = 1$ \textbf{to} $\textsc{Size}(population)$}
		\State $x \gets \textsc{TournamentSelect}(population)$
		\State $y \gets \textsc{TournamentSelect}(population)$
		\State $child \gets \textsc{Reproduce}(x, y)$
		\If{$mutateChance$}
			\State $\textsc{Mutate}(child)$
		\EndIf
		\State add $child$ to $newPopulation$
	\EndFor
	\State $population \gets newPopulation$
\EndWhile
\State \Return the element of $population$ that is a consistent $k$-coloring of $G$
\end{algorithmic}
\end{algorithm}
\paragraph{}
The design decisions we needed to make consisted of:
\begin{itemize}
\item
How to implement \textsc{GenerateInitialPopulation}

\item
How to implement \textsc{TournamentSelect}

\item
How to implement \textsc{Reproduce}, including whether to return one child or two.

\item
How to implement \textsc{Mutate}
\end{itemize}
\paragraph{}
When implementing \textsc{GenerateInitialPopulation}, we decided that simpler is better and faster, and used a number of randomized colorings to initialize the population. Each individual consists of a random color assignment to each vertex, regardless of neighboring vertices. The number of individuals in the population is set as a tuning parameter.
\paragraph{}
Implementing the parent selection was relatively simple, as the project requirements specified we must use a tournament selection method. When implementing \textsc{TournamentSelect}, both the tournament size and the chance of selecting the strongest individual are exposed as tuning parameters.
\paragraph{}
The fitness function used to determine the fitness of an individual for selection is simply the number of conflicts in that coloring. A perfect individual is one who this function returns 0 for. Because we are trying to minimize the number of conflicts, a more ``fit'' individual is one who this fitness function returns a \textit{lower} number for. \textsc{TournamentSelect} therefore selects based on lowest value, and in fact, the entire algorithm is trying to minimize this value.
\paragraph{}
When implementing \textsc{Reproduce}, we chose a randomized one-point crossover that produces only 1 child. A random split point is selected, and a child is created using assignments from one parent on one side of the split, and assignments from the other parent on the other side. It is important to note that because the problem generator outputs vertices sorted by x-coordinate and the sequence of color assignments follows the same order as the vertices, this crossover roughly corresponds to a crossover in the graph itself. That means the reproduction process should only create or resolve conflicts along the edges cut at the crossover point.
\paragraph{}
To mutate the children, \textsc{Mutate} selects a random assignment in the coloring and setting it to a random value. This simple approach should add enough variation and diversity to ``climb over'' any local extrema. The chance for a mutation to occur is exposed as a tuning parameter.
\paragraph{}
The genetic algorithm will be implemented in Python 3, using a single function that defines additional internal functions for fitness, tournament selection, reproduction, and mutation. Graphs to be used can either be generated immediately by the problem generator, or loaded from saved files exported by the problem generator.

\section{Experimental Design}
\paragraph{}
Each of our algorithms will be run on graphs of 10 different sizes a number of times. For the Min-Conflicts algorithm, we will execute multiple runs on the same graph and calculate the average time to find a solution as well as the rate of a solution being found at all. For the Backtracking algorithms, we will run the algorithm multiple different graphs due to their deterministic nature. With the Genetic Algorithm, we will perform multiple runs on the same graph as we did with, but in this case we will be altering the various tuning parameters with each set of runs to monitor how runtime is affected.

\end{document}